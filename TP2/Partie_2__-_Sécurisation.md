# Partie 2 : Sécurisation

## Sommaire

[[_TOC_]]

```bash
[yoann@web /]$ sudo cat /etc/ssh/sshd_config
[sudo] password for yoann:
#       $OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
#Port 22
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::

HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# This system is following system-wide crypto policy. The changes to
# crypto properties (Ciphers, MACs, ...) will not have any effect here.
# They will be overridden by command-line options passed to the server
# on command line.
# Please, check manual pages for update-crypto-policies(8) and sshd_config(5).

# Logging
#SyslogFacility AUTH
SyslogFacility AUTHPRIV
#LogLevel INFO

# Authentication:

#LoginGraceTime 2m
#PermitRootLogin no
StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

PubkeyAuthentication yes

# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile      .ssh/authorized_keys

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication yes
#PermitEmptyPasswords no
#PasswordAuthentication no

# Change to no to disable s/key passwords
#ChallengeResponseAuthentication yes
ChallengeResponseAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no
#KerberosUseKuserok yes

# GSSAPI options
GSSAPIAuthentication yes
GSSAPICleanupCredentials no
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no
#GSSAPIEnablek5users no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
# WARNING: 'UsePAM no' is not supported in Fedora and may cause several
# problems.
UsePAM yes

#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
X11Forwarding yes
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes

# It is recommended to use pam_motd in /etc/pam.d/sshd instead of PrintMotd,
# as it is more configurable and versatile than the built-in version.
PrintMotd no

#PrintLastLog yes
#TCPKeepAlive yes
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
#UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# Accept locale-related environment variables
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS

# override default of no subsystems
#Subsystem      sftp    /usr/libexec/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#       X11Forwarding no
#       AllowTcpForwarding no
#       PermitTTY no
#       ForceCommand cvs server

## Hardening
UseDNS no
VersionAddendum pancho
PasswordAuthentication no
PermitRootLogin prohibit-password
LogLevel VERBOSE
AuthenticationMethods publickey

# more logging for sftp sessiosn
Subsystem sftp /usr/libexec/sftp-server -f AUTHPRIV -l INFO

# algorithms
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
hostkeyalgorithms ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521-cert-v01@openssh.com,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-ed25519,rsa-sha2-512,rsa-sha2-256
```

```bash
[yoann@web ~]$ sudo zgrep 'Ban' /var/log/fail2ban.log*
[sudo] password for yoann:
2021-12-14 15:27:09,731 fail2ban.actions        [11558]: NOTICE  [sshd] Ban 10.2.1.1
```

```bash
[yoann@proxy ~]$ sudo dnf list | grep nginx
[sudo] password for yoann:
nginx.x86_64                                           1:1.14.1-9.module+el8.4.0+542+81547229            @appstream
nginx-all-modules.noarch                               1:1.14.1-9.module+el8.4.0+542+81547229            @appstream
nginx-filesystem.noarch                                1:1.14.1-9.module+el8.4.0+542+81547229            @appstream
nginx-mod-http-image-filter.x86_64                     1:1.14.1-9.module+el8.4.0+542+81547229            @appstream
nginx-mod-http-perl.x86_64                             1:1.14.1-9.module+el8.4.0+542+81547229            @appstream
nginx-mod-http-xslt-filter.x86_64                      1:1.14.1-9.module+el8.4.0+542+81547229            @appstream
nginx-mod-mail.x86_64                                  1:1.14.1-9.module+el8.4.0+542+81547229            @appstream
nginx-mod-stream.x86_64                                1:1.14.1-9.module+el8.4.0+542+81547229            @appstream
pcp-pmda-nginx.x86_64                                  5.3.1-5.el8                                       appstream
```

```bash
[yoann@proxy ~]$ [yoann@proxy ~]$ cat /etc/nginx/nginx.conf
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }

# Settings for a TLS enabled server.
#
#    server {
#        listen       443 ssl http2 default_server;
#        listen       [::]:443 ssl http2 default_server;
#        server_name  _;
#        root         /usr/share/nginx/html;
#
#        ssl_certificate "/etc/pki/nginx/server.crt";
#        ssl_certificate_key "/etc/pki/nginx/private/server.key";
#        ssl_session_cache shared:SSL:1m;
#        ssl_session_timeout  10m;
#        ssl_ciphers PROFILE=SYSTEM;
#        ssl_prefer_server_ciphers on;
#
#        # Load configuration files for the default server block.
#        include /etc/nginx/default.d/*.conf;
#
#        location / {
#        }
#
#        error_page 404 /404.html;
#            location = /40x.html {
#        }
#
#        error_page 500 502 503 504 /50x.html;
#            location = /50x.html {
#        }
#    }

}
```

```bash
[yoann@node1 certs]$ curl -L -k web.tp2.cesi
<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="en" data-locale="en" >
        <head
 data-requesttoken="QV1ITLOpilNASz9SL9YvIrFBsts5zEuBKJBPnU4rIBk=:MBs5K/b85QE0KWkAbuceV4EzgJlWtX3tXtsE6ixYEHM=">
                <meta charset="utf-8">
                <title>
                Nextcloud               </title>
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
                                <meta name="apple-itunes-app" content="app-id=1125420102">
                                <meta name="theme-color" content="#0082c9">
                <link rel="icon" href="/core/img/favicon.ico">
                <link rel="apple-touch-icon" href="/core/img/favicon-touch.png">
                <link rel="mask-icon" sizes="any" href="/core/img/favicon-mask.svg" color="#0082c9">
                <link rel="manifest" href="/index.php/apps/theming/manifest?v=0">
                <link rel="stylesheet" href="/apps/files_rightclick/css/app.css?v=47cd76e4-0">
<link rel="stylesheet" href="/core/css/guest.css?v=f819fbe9-0">
                <script nonce="UVYxSVRMT3BpbE5BU3o5U0w5WXZJckZCc3RzNXpFdUJLSkJQblU0cklCaz06TUJzNUsvYjg1UUUwS1drQWJ1Y2VWNEV6Z0psV3RYM3RYdHNFNml4WUVITT0=" defer src="/index.php/core/js/oc.js?v=f819fbe9"></script>
<script nonce="UVYxSVRMT3BpbE5BU3o5U0w5WXZJckZCc3RzNXpFdUJLSkJQblU0cklCaz06TUJzNUsvYjg1UUUwS1drQWJ1Y2VWNEV6Z0psV3RYM3RYdHNFNml4WUVITT0=" defer src="/core/js/dist/main.js?v=f819fbe9-0"></script>
<script nonce="UVYxSVRMT3BpbE5BU3o5U0w5WXZJckZCc3RzNXpFdUJLSkJQblU0cklCaz06TUJzNUsvYjg1UUUwS1drQWJ1Y2VWNEV6Z0psV3RYM3RYdHNFNml4WUVITT0=" defer src="/core/js/dist/files_fileinfo.js?v=f819fbe9-0"></script>
<script nonce="UVYxSVRMT3BpbE5BU3o5U0w5WXZJckZCc3RzNXpFdUJLSkJQblU0cklCaz06TUJzNUsvYjg1UUUwS1drQWJ1Y2VWNEV6Z0psV3RYM3RYdHNFNml4WUVITT0=" defer src="/core/js/dist/files_client.js?v=f819fbe9-0"></script>
<script nonce="UVYxSVRMT3BpbE5BU3o5U0w5WXZJckZCc3RzNXpFdUJLSkJQblU0cklCaz06TUJzNUsvYjg1UUUwS1drQWJ1Y2VWNEV6Z0psV3RYM3RYdHNFNml4WUVITT0=" defer src="/index.php/js/core/merged-template-prepend.js?v=f819fbe9-0"></script>
<script nonce="UVYxSVRMT3BpbE5BU3o5U0w5WXZJckZCc3RzNXpFdUJLSkJQblU0cklCaz06TUJzNUsvYjg1UUUwS1drQWJ1Y2VWNEV6Z0psV3RYM3RYdHNFNml4WUVITT0=" defer src="/core/js/backgroundjobs.js?v=f819fbe9-0"></script>
<script nonce="UVYxSVRMT3BpbE5BU3o5U0w5WXZJckZCc3RzNXpFdUJLSkJQblU0cklCaz06TUJzNUsvYjg1UUUwS1drQWJ1Y2VWNEV6Z0psV3RYM3RYdHNFNml4WUVITT0=" defer src="/apps/files_sharing/js/dist/main.js?v=f819fbe9-0"></script>
<script nonce="UVYxSVRMT3BpbE5BU3o5U0w5WXZJckZCc3RzNXpFdUJLSkJQblU0cklCaz06TUJzNUsvYjg1UUUwS1drQWJ1Y2VWNEV6Z0psV3RYM3RYdHNFNml4WUVITT0=" defer src="/apps/files_videoplayer/js/main.js?v=f819fbe9-0"></script>
<script nonce="UVYxSVRMT3BpbE5BU3o5U0w5WXZJckZCc3RzNXpFdUJLSkJQblU0cklCaz06TUJzNUsvYjg1UUUwS1drQWJ1Y2VWNEV6Z0psV3RYM3RYdHNFNml4WUVITT0=" defer src="/apps/files_rightclick/js/script.js?v=f819fbe9-0"></script>
<script nonce="UVYxSVRMT3BpbE5BU3o5U0w5WXZJckZCc3RzNXpFdUJLSkJQblU0cklCaz06TUJzNUsvYjg1UUUwS1drQWJ1Y2VWNEV6Z0psV3RYM3RYdHNFNml4WUVITT0=" defer src="/apps/files_rightclick/js/files.js?v=f819fbe9-0"></script>
<script nonce="UVYxSVRMT3BpbE5BU3o5U0w5WXZJckZCc3RzNXpFdUJLSkJQblU0cklCaz06TUJzNUsvYjg1UUUwS1drQWJ1Y2VWNEV6Z0psV3RYM3RYdHNFNml4WUVITT0=" defer src="/apps/theming/js/theming.js?v=f819fbe9-0"></script>
<script nonce="UVYxSVRMT3BpbE5BU3o5U0w5WXZJckZCc3RzNXpFdUJLSkJQblU0cklCaz06TUJzNUsvYjg1UUUwS1drQWJ1Y2VWNEV6Z0psV3RYM3RYdHNFNml4WUVITT0=" defer src="/core/js/dist/login.js?v=f819fbe9-0"></script>
                <link rel="stylesheet" media="(prefers-color-scheme: dark)" href="/index.php/apps/accessibility/css/user-a82fd95db10ff25dfad39f07372ebe37"/><meta property="og:title" content="Nextcloud"/><meta property="og:description" content="a safe home for all your data"/><meta property="og:site_name" content="Nextcloud"/><meta property="og:url" content="http://web.tp2.cesi/"/><meta property="og:type" content="website"/><meta property="og:image" content="http://web.tp2.cesi/core/img/favicon-touch.png"/><link rel="stylesheet" href="/index.php/apps/theming/styles?v=0"/><meta name="robots" content="noindex, nofollow"/>        </head>
        <body id="body-login">
                <noscript>
        <div id="nojavascript">
                <div>
                        This application requires JavaScript for correct operation. Please <a href="https://www.enable-javascript.com/" target="_blank" rel="noreferrer noopener">enable JavaScript</a> and reload the page.          </div>
        </div>
</noscript>
                                        <input type="hidden" id="initial-state-core-loginUsername" value="IiI=">
                                        <input type="hidden" id="initial-state-core-loginAutocomplete" value="dHJ1ZQ==">
                                        <input type="hidden" id="initial-state-core-loginThrottleDelay" value="MA==">
                                        <input type="hidden" id="initial-state-core-loginResetPasswordLink" value="IiI=">
                                        <input type="hidden" id="initial-state-core-loginCanResetPassword" value="dHJ1ZQ==">
                                        <input type="hidden" id="initial-state-core-webauthn-available" value="dHJ1ZQ==">
                                        <input type="hidden" id="initial-state-core-config" value="eyJzZXNzaW9uX2xpZmV0aW1lIjoxNDQwLCJzZXNzaW9uX2tlZXBhbGl2ZSI6dHJ1ZSwiYXV0b19sb2dvdXQiOmZhbHNlLCJ2ZXJzaW9uIjoiMjEuMC4xLjEiLCJ2ZXJzaW9uc3RyaW5nIjoiMjEuMC4xIiwiZW5hYmxlX2F2YXRhcnMiOnRydWUsImxvc3RfcGFzc3dvcmRfbGluayI6bnVsbCwibW9kUmV3cml0ZVdvcmtpbmciOmZhbHNlLCJzaGFyaW5nLm1heEF1dG9jb21wbGV0ZVJlc3VsdHMiOjI1LCJzaGFyaW5nLm1pblNlYXJjaFN0cmluZ0xlbmd0aCI6MCwiYmxhY2tsaXN0X2ZpbGVzX3JlZ2V4IjoiXFwuKHBhcnR8ZmlsZXBhcnQpJCJ9">
                                        <input type="hidden" id="initial-state-core-capabilities" value="eyJjb3JlIjp7InBvbGxpbnRlcnZhbCI6NjAsIndlYmRhdi1yb290IjoicmVtb3RlLnBocFwvd2ViZGF2In0sImJydXRlZm9yY2UiOnsiZGVsYXkiOjB9LCJmaWxlcyI6eyJiaWdmaWxlY2h1bmtpbmciOnRydWUsImJsYWNrbGlzdGVkX2ZpbGVzIjpbIi5odGFjY2VzcyJdLCJkaXJlY3RFZGl0aW5nIjp7InVybCI6Imh0dHA6XC9cL3dlYi50cDIuY2VzaVwvb2NzXC92Mi5waHBcL2FwcHNcL2ZpbGVzXC9hcGlcL3YxXC9kaXJlY3RFZGl0aW5nIiwiZXRhZyI6IjYyMjZiYTg3MzM3M2Y1ZTczYTNlZjUwNDEwNzUyM2Y3In0sImNvbW1lbnRzIjp0cnVlLCJ1bmRlbGV0ZSI6dHJ1ZSwidmVyc2lvbmluZyI6dHJ1ZX0sImFjdGl2aXR5Ijp7ImFwaXYyIjpbImZpbHRlcnMiLCJmaWx0ZXJzLWFwaSIsInByZXZpZXdzIiwicmljaC1zdHJpbmdzIl19LCJvY20iOnsiZW5hYmxlZCI6dHJ1ZSwiYXBpVmVyc2lvbiI6IjEuMC1wcm9wb3NhbDEiLCJlbmRQb2ludCI6Imh0dHA6XC9cL3dlYi50cDIuY2VzaVwvaW5kZXgucGhwXC9vY20iLCJyZXNvdXJjZVR5cGVzIjpbeyJuYW1lIjoiZmlsZSIsInNoYXJlVHlwZXMiOlsidXNlciIsImdyb3VwIl0sInByb3RvY29scyI6eyJ3ZWJkYXYiOiJcL3B1YmxpYy5waHBcL3dlYmRhdlwvIn19XX0sImRhdiI6eyJjaHVua2luZyI6IjEuMCJ9LCJub3RpZmljYXRpb25zIjp7Im9jcy1lbmRwb2ludHMiOlsibGlzdCIsImdldCIsImRlbGV0ZSIsImRlbGV0ZS1hbGwiLCJpY29ucyIsInJpY2gtc3RyaW5ncyIsImFjdGlvbi13ZWIiLCJ1c2VyLXN0YXR1cyJdLCJwdXNoIjpbImRldmljZXMiLCJvYmplY3QtZGF0YSIsImRlbGV0ZSJdLCJhZG1pbi1ub3RpZmljYXRpb25zIjpbIm9jcyIsImNsaSJdfSwicGFzc3dvcmRfcG9saWN5Ijp7Im1pbkxlbmd0aCI6OCwiZW5mb3JjZU5vbkNvbW1vblBhc3N3b3JkIjp0cnVlLCJlbmZvcmNlTnVtZXJpY0NoYXJhY3RlcnMiOmZhbHNlLCJlbmZvcmNlU3BlY2lhbENoYXJhY3RlcnMiOmZhbHNlLCJlbmZvcmNlVXBwZXJMb3dlckNhc2UiOmZhbHNlLCJhcGkiOnsiZ2VuZXJhdGUiOiJodHRwOlwvXC93ZWIudHAyLmNlc2lcL29jc1wvdjIucGhwXC9hcHBzXC9wYXNzd29yZF9wb2xpY3lcL2FwaVwvdjFcL2dlbmVyYXRlIiwidmFsaWRhdGUiOiJodHRwOlwvXC93ZWIudHAyLmNlc2lcL29jc1wvdjIucGhwXC9hcHBzXC9wYXNzd29yZF9wb2xpY3lcL2FwaVwvdjFcL3ZhbGlkYXRlIn19LCJwcm92aXNpb25pbmdfYXBpIjp7InZlcnNpb24iOiIxLjExLjAiLCJBY2NvdW50UHJvcGVydHlTY29wZXNWZXJzaW9uIjoyLCJBY2NvdW50UHJvcGVydHlTY29wZXNGZWRlcmF0aW9uRW5hYmxlZCI6dHJ1ZX0sImZpbGVzX3NoYXJpbmciOnsic2hhcmVieW1haWwiOnsiZW5hYmxlZCI6dHJ1ZSwidXBsb2FkX2ZpbGVzX2Ryb3AiOnsiZW5hYmxlZCI6dHJ1ZX0sInBhc3N3b3JkIjp7ImVuYWJsZWQiOnRydWUsImVuZm9yY2VkIjpmYWxzZX0sImV4cGlyZV9kYXRlIjp7ImVuYWJsZWQiOnRydWV9fSwiYXBpX2VuYWJsZWQiOnRydWUsInB1YmxpYyI6eyJlbmFibGVkIjp0cnVlLCJwYXNzd29yZCI6eyJlbmZvcmNlZCI6ZmFsc2UsImFza0Zvck9wdGlvbmFsUGFzc3dvcmQiOmZhbHNlfSwiZXhwaXJlX2RhdGUiOnsiZW5hYmxlZCI6ZmFsc2V9LCJtdWx0aXBsZV9saW5rcyI6dHJ1ZSwiZXhwaXJlX2RhdGVfaW50ZXJuYWwiOnsiZW5hYmxlZCI6ZmFsc2V9LCJzZW5kX21haWwiOmZhbHNlLCJ1cGxvYWQiOnRydWUsInVwbG9hZF9maWxlc19kcm9wIjp0cnVlfSwicmVzaGFyaW5nIjp0cnVlLCJ1c2VyIjp7InNlbmRfbWFpbCI6ZmFsc2UsImV4cGlyZV9kYXRlIjp7ImVuYWJsZWQiOnRydWV9fSwiZ3JvdXBfc2hhcmluZyI6dHJ1ZSwiZ3JvdXAiOnsiZW5hYmxlZCI6dHJ1ZSwiZXhwaXJlX2RhdGUiOnsiZW5hYmxlZCI6dHJ1ZX19LCJkZWZhdWx0X3Blcm1pc3Npb25zIjozMSwiZmVkZXJhdGlvbiI6eyJvdXRnb2luZyI6dHJ1ZSwiaW5jb21pbmciOnRydWUsImV4cGlyZV9kYXRlIjp7ImVuYWJsZWQiOnRydWV9fSwic2hhcmVlIjp7InF1ZXJ5X2xvb2t1cF9kZWZhdWx0IjpmYWxzZSwiYWx3YXlzX3Nob3dfdW5pcXVlIjp0cnVlfX0sInRoZW1pbmciOnsibmFtZSI6Ik5leHRjbG91ZCIsInVybCI6Imh0dHBzOlwvXC9uZXh0Y2xvdWQuY29tIiwic2xvZ2FuIjoiYSBzYWZlIGhvbWUgZm9yIGFsbCB5b3VyIGRhdGEiLCJjb2xvciI6IiMwMDgyYzkiLCJjb2xvci10ZXh0IjoiI2ZmZmZmZiIsImNvbG9yLWVsZW1lbnQiOiIjMDA4MmM5IiwiY29sb3ItZWxlbWVudC1icmlnaHQiOiIjMDA4MmM5IiwiY29sb3ItZWxlbWVudC1kYXJrIjoiIzAwODJjOSIsImxvZ28iOiJodHRwOlwvXC93ZWIudHAyLmNlc2lcL2NvcmVcL2ltZ1wvbG9nb1wvbG9nby5zdmc/dj0wIiwiYmFja2dyb3VuZCI6Imh0dHA6XC9cL3dlYi50cDIuY2VzaVwvY29yZVwvaW1nXC9iYWNrZ3JvdW5kLnBuZz92PTAiLCJiYWNrZ3JvdW5kLXBsYWluIjpmYWxzZSwiYmFja2dyb3VuZC1kZWZhdWx0Ijp0cnVlLCJsb2dvaGVhZGVyIjoiaHR0cDpcL1wvd2ViLnRwMi5jZXNpXC9jb3JlXC9pbWdcL2xvZ29cL2xvZ28uc3ZnP3Y9MCIsImZhdmljb24iOiJodHRwOlwvXC93ZWIudHAyLmNlc2lcL2NvcmVcL2ltZ1wvbG9nb1wvbG9nby5zdmc/dj0wIn0sInVzZXJfc3RhdHVzIjp7ImVuYWJsZWQiOnRydWUsInN1cHBvcnRzX2Vtb2ppIjp0cnVlfSwid2VhdGhlcl9zdGF0dXMiOnsiZW5hYmxlZCI6dHJ1ZX19">
                                        <input type="hidden" id="initial-state-theming-data" value="eyJuYW1lIjoiTmV4dGNsb3VkIiwidXJsIjoiaHR0cHM6XC9cL25leHRjbG91ZC5jb20iLCJzbG9nYW4iOiJhIHNhZmUgaG9tZSBmb3IgYWxsIHlvdXIgZGF0YSIsImNvbG9yIjoiIzAwODJjOSIsImltcHJpbnRVcmwiOiIiLCJwcml2YWN5VXJsIjoiIiwiaW52ZXJ0ZWQiOmZhbHNlLCJjYWNoZUJ1c3RlciI6IjAifQ==">
                                        <input type="hidden" id="initial-state-accessibility-data" value="eyJ0aGVtZSI6ZmFsc2UsImhpZ2hjb250cmFzdCI6ZmFsc2V9">
                                <div class="wrapper">
                        <div class="v-align">
                                                                        <header role="banner">
                                                <div id="header">
                                                        <div class="logo">
                                                                <h1 class="hidden-visually">
                                                                        Nextcloud                                                               </h1>
                                                                                                                        </div>
                                                </div>
                                        </header>
                                                                <main>

<div id="login"></div>

                                </main>
                        </div>
                </div>
                <footer role="contentinfo">
                        <p class="info">
                                <a href="https://nextcloud.com" target="_blank" rel="noreferrer noopener" class="entity-name">Nextcloud</a> – a safe home for all your data                     </p>
                </footer>
        </body>
</html>
[yoann@node1 certs]$ [yoann@node1 certs]$ curl -L -k 10.2.1.13
 data-requesttoken="njpYSTf9+zNEyqQ/X9zzzovFj5RqSbTXdD37WaB2klo=:0HU/eFrLl2AuochtLYuAmMmxxqAtecyPN3CaNpUZ1ms=">
                <meta charset="utf-8">
                <title>
                Nextcloud               </title>
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
                                <meta name="apple-itunes-app" content="app-id=1125420102">
                                <meta name="theme-color" content="#0082c9">
                <link rel="icon" href="/core/img/favicon.ico">
                <link rel="apple-touch-icon" href="/core/img/favicon-touch.png">
                <link rel="mask-icon" sizes="any" href="/core/img/favicon-mask.svg" color="#0082c9">
-bash: [yoann@node1: command not found
                <link rel="manifest" href="/index.php/apps/theming/manifest?v=0">
                <link rel="stylesheet" href="/apps/files_rightclick/css/app.css?v=47cd76e4-0">
<link rel="stylesheet" href="/core/css/guest.css?v=f819fbe9-0">
                <script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/index.php/core/js/oc.js?v=f819fbe9"></script>
[yoann@node1 certs]$ <!DOCTYPE html>
<script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/core/js/dist/main.js?v=f819fbe9-0"></script>
-bash: !DOCTYPE: event not found
[yoann@node1 certs]$ <html class="ng-csp" data-placeholder-focus="false" lang="en" data-locale="en" >
<script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/core/js/dist/files_fileinfo.js?v=f819fbe9-0"></script>
<script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/core/js/dist/files_client.js?v=f819fbe9-0"></script>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$         <head
<script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/index.php/js/core/merged-template-prepend.js?v=f819fbe9-0"></script>
-bash: head: No such file or directory
[yoann@node1 certs]$  data-requesttoken="njpYSTf9+zNEyqQ/X9zzzovFj5RqSbTXdD37WaB2klo=:0HU/eFrLl2AuochtLYuAmMmxxqAtecyPN3CaNpUZ1ms=">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                 <meta charset="utf-8">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                 <title>
<script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0-bash: syntax error near unexpected token `newline'
=" defer src="/apps/files_rightclick/js/script.js?v=f819fbe9-0"></script>
<script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/apps/files_rightclick/js/files.js?v=f819fbe9-0"></script>
[yoann@node1 certs]$                 Nextcloud               </title>
-bash: syntax error near unexpected token `newline'
<script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/apps/theming/js/theming.js?v=f819fbe9-0"></script>
[yoann@node1 certs]$                 <meta http-equiv="X-UA-Compatible" content="IE=edge">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                 <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
-bash: syntax error near unexpected token `newline'
<script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/core/js/dist/login.js?v=f819fbe9-0"></script>
                <link rel="stylesheet" media="(prefers-color-scheme: dark)" href="/index.php/apps/accessibility/css/user-a82fd95db10ff25dfad39f07372ebe37"/><meta property="og:title" content="Nextcloud"/><meta property="og:description" content="a safe home for all your data"/><meta property="og:site_name" content="Nextcloud"/><meta property="og:url" content="http://web.tp2.cesi/"/><meta property="og:type" content="website"/><meta property="og:image" content="http://web.tp2.cesi/core/img/favicon-touch.png"/><link rel="stylesheet" href="/index.php/apps/theming/styles?v=0"/><meta name="robots" content="noindex, nofollow"/>        </head>
[yoann@node1 certs]$                                 <meta name="apple-itunes-app" content="app-id=1125420102">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                 <meta name="theme-color" content="#0082c9">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                 <link rel="icon" href="/core/img/favicon.ico">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                 <link rel="apple-touch-icon" href="/core/img/favicon-touch.png">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                 <link rel="mask-icon" sizes="any" href="/core/img/favicon-mask.svg" color="#0082c9">
-bash: syntax error near unexpected token `newline'
                        This application requires JavaScript for correct operation. Please <a href="https://www.enable-javascript.com/" target="_blank" rel="noreferrer noopener">enable JavaScript</a> and reload the page.          </div>
        </div>
[yoann@node1 certs]$                 <link rel="manifest" href="/index.php/apps/theming/manifest?v=0">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                 <link rel="stylesheet" href="/apps/files_rightclick/css/app.css?v=47cd76e4-0">
-bash: syntax error near unexpected token `newline'
                                        <input type="hidden" id="initial-state-core-loginAutocomplete" value="dHJ1ZQ==">
[yoann@node1 certs]$ <link rel="stylesheet" href="/core/css/guest.css?v=f819fbe9-0">
-bash: syntax error near unexpected token `newline'
                                        <input type="hidden" id="initial-state-core-loginThrottleDelay" value="MA==">
                                        <input type="hidden" id="initial-state-core-loginResetPasswordLink" value="IiI=">
[yoann@node1 certs]$                 <script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/index.php/core/js/oc.js?v=f819fbe9"></script>
-bash: syntax error near unexpected token `<'
[yoann@node1 certs]$ <script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/core/js/dist/main.js?v=f819fbe9-0"></script>
-bash: syntax error near unexpected token `<'
[yoann@node1 certs]$ <script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/core/js/dist/files_fileinfo.js?v=f819fbe9-0"></script>
-bash: syntax error near unexpected token `<'
[yoann@node1 certs]$ <script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/core/js/dist/files_client.js?v=f819fbe9-0"></script>
-bash: syntax error near unexpected token `<'
[yoann@node1 certs]$ <script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/index.php/js/core/merged-template-prepend.js?v=f819fbe9-0"></script>
-bash: syntax error near unexpected token `<'
[yoann@node1 certs]$ <script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/core/js/backgroundjobs.js?v=f819fbe9-0"></script>
-bash: syntax error near unexpected token `<'
[yoann@node1 certs]$ <script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/apps/files_sharing/js/dist/main.js?v=f819fbe9-0"></script>
-bash: syntax error near unexpected token `<'
[yoann@node1 certs]$ <script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/apps/files_videoplayer/js/main.js?v=f819fbe9-0"></script>
-bash: syntax error near unexpected token `<'
[yoann@node1 certs]$ <script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/apps/files_rightclick/js/script.js?v=f819fbe9-0"></script>
-bash: syntax error near unexpected token `<'
[yoann@node1 certs]$ <script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/apps/files_rightclick/js/files.js?v=f819fbe9-0"></script>
-bash: syntax error near unexpected token `<'
[yoann@node1 certs]$ <script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/apps/theming/js/theming.js?v=f819fbe9-0"></script>
-bash: syntax error near unexpected token `<'
[yoann@node1 certs]$ <script nonce="bmpwWVNUZjkrek5FeXFRL1g5enp6b3ZGajVScVNiVFhkRDM3V2FCMmtsbz06MEhVL2VGckxsMkF1b2NodExZdUFtTW14eHFBdGVjeVBOM0NhTnBVWjFtcz0=" defer src="/core/js/dist/login.js?v=f819fbe9-0"></script>
-bash: syntax error near unexpected token `<'
[yoann@node1 certs]$                 <link rel="stylesheet" media="(prefers-color-scheme: dark)" href="/index.php/apps/accessibility/css/user-a82fd95db10ff25dfad39f07372ebe37"/><meta property="og:title" content="Nextcloud"/><meta property="og:description" content="a safe home for all your data"/><meta property="og:site_name" content="Nextcloud"/><meta property="og:url" content="http://web.tp2.cesi/"/><meta property="og:type" content="website"/><meta property="og:image" content="http://web.tp2.cesi/core/img/favicon-touch.png"/><link rel="stylesheet" href="/index.php/apps/theming/styles?v=0"/><meta name="robots" content="noindex, nofollow"/>        </head>
-bash: syntax error near unexpected token `<'
[yoann@node1 certs]$         <body id="body-login">
sImV4cGlyZV9kYXRlIjp7ImVuYWJsZW-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                 <noscript>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$         <div id="nojavascript">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                 <div>
XJ-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                         This application requires JavaScript for correct operation. Please <a href="https://www.enable-javascript.com/" target="_blank" rel="noreferrer noopener">enable JavaScript</a> and reload the page.          </div>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$         </div>
-bash: syntax error near unexpected token `newline'
b3dfdW5pcXVlIjp[yoann@node1 certs]$ </noscript>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                         <input type="hidden" id="initial-state-core-loginUsername" value="IiI=">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                         <input type="hidden" id="initial-state-core-loginAutocomplete" value="dHJ1ZQ==">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                         <input type="hidden" id="initial-state-core-loginThrottleDelay" value="MA==">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                         <input type="hidden" id="initial-state-core-loginResetPasswordLink" value="IiI=">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                         <input type="hidden" id="initial-state-core-loginCanResetPassword" value="dHJ1ZQ==">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                         <input type="hidden" id="initial-state-core-webauthn-available" value="dHJ1ZQ==">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                         <input type="hidden" id="initial-state-core-config" value="eyJzZXNzaW9uX2xpZmV0aW1lIjoxNDQwLCJzZXNzaW9uX2tlZXBhbGl2ZSI6dHJ1ZSwiYXV0b19sb2dvdXQiOmZhbHNlLCJ2ZXJzaW9uIjoiMjEuMC4xLjEiLCJ2ZXJzaW9uc3RyaW5nIjoiMjEuMC4xIiwiZW5hYmxlX2F2YXRhcnMiOnRydWUsImxvc3RfcGFzc3dvcmRfbGluayI6bnVsbCwibW9kUmV3cml0ZVdvcmtpbmciOmZhbHNlLCJzaGFyaW5nLm1heEF1dG9jb21wbGV0ZVJlc3VsdHMiOjI1LCJzaGFyaW5nLm1pblNlYXJjaFN0cmluZ0xlbmd0aCI6MCwiYmxhY2tsaXN0X2ZpbGVzX3JlZ2V4IjoiXFwuKHBhcnR8ZmlsZXBhcnQpJCJ9">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                         <input type="hidden" id="initial-state-core-capabilities" value="eyJjb3JlIjp7InBvbGxpbnRlcnZhbCI6NjAsIndlYmRhdi1yb290IjoicmVtb3RlLnBocFwvd2ViZGF2In0sImJydXRlZm9yY2UiOnsiZGVsYXkiOjB9LCJmaWxlcyI6eyJiaWdmaWxlY2h1bmtpbmciOnRydWUsImJsYWNrbGlzdGVkX2ZpbGVzIjpbIi5odGFjY2VzcyJdLCJkaXJlY3RFZGl0aW5nIjp7InVybCI6Imh0dHA6XC9cL3dlYi50cDIuY2VzaVwvb2NzXC92Mi5waHBcL2FwcHNcL2ZpbGVzXC9hcGlcL3YxXC9kaXJlY3RFZGl0aW5nIiwiZXRhZyI6IjYyMjZiYTg3MzM3M2Y1ZTczYTNlZjUwNDEwNzUyM2Y3In0sImNvbW1lbnRzIjp0cnVlLCJ1bmRlbGV0ZSI6dHJ1ZSwidmVyc2lvbmluZyI6dHJ1ZX0sImFjdGl2aXR5Ijp7ImFwaXYyIjpbImZpbHRlcnMiLCJmaWx0ZXJzLWFwaSIsInByZXZpZXdzIiwicmljaC1zdHJpbmdzIl19LCJvY20iOnsiZW5hYmxlZCI6dHJ1ZSwiYXBpVmVyc2lvbiI6IjEuMC1wcm9wb3NhbDEiLCJlbmRQb2ludCI6Imh0dHA6XC9cL3dlYi50cDIuY2VzaVwvaW5kZXgucGhwXC9vY20iLCJyZXNvdXJjZVR5cGVzIjpbeyJuYW1lIjoiZmlsZSIsInNoYXJlVHlwZXMiOlsidXNlciIsImdyb3VwIl0sInByb3RvY29scyI6eyJ3ZWJkYXYiOiJcL3B1YmxpYy5waHBcL3dlYmRhdlwvIn19XX0sImRhdiI6eyJjaHVua2luZyI6IjEuMCJ9LCJub3RpZmljYXRpb25zIjp7Im9jcy1lbmRwb2ludHMiOlsibGlzdCIsImdldCIsImRlbGV0ZSIsImRlbGV0ZS1hbGwiLCJpY29ucyIsInJpY2gtc3RyaW5ncyIsImFjdGlvbi13ZWIiLCJ1c2VyLXN0YXR1cyJdLCJwdXNoIjpbImRldmljZXMiLCJvYmplY3QtZGF0YSIsImRlbGV0ZSJdLCJhZG1pbi1ub3RpZmljYXRpb25zIjpbIm9jcyIsImNsaSJdfSwicGFzc3dvcmRfcG9saWN5Ijp7Im1pbkxlbmd0aCI6OCwiZW5mb3JjZU5vbkNvbW1vblBhc3N3b3JkIjp0cnVlLCJlbmZvcmNlTnVtZXJpY0NoYXJhY3RlcnMiOmZhbHNlLCJlbmZvcmNlU3BlY2lhbENoYXJhY3RlcnMiOmZhbHNlLCJlbmZvcmNlVXBwZXJMb3dlckNhc2UiOmZhbHNlLCJhcGkiOnsiZ2VuZXJhdGUiOiJodHRwOlwvXC93ZWIudHAyLmNlc2lcL29jc1wvdjIucGhwXC9hcHBzXC9wYXNzd29yZF9wb2xpY3lcL2FwaVwvdjFcL2dlbmVyYXRlIiwidmFsaWRhdGUiOiJodHRwOlwvXC93ZWIudHAyLmNlc2lcL29jc1wvdjIucGhwXC9hcHBzXC9wYXNzd29yZF9wb2xpY3lcL2FwaVwvdjFcL3ZhbGlkYXRlIn19LCJwcm92aXNpb25pbmdfYXBpIjp7InZlcnNpb24iOiIxLjExLjAiLCJBY2NvdW50UHJvcGVydHlTY29wZXNWZXJzaW9uIjoyLCJBY2NvdW50UHJvcGVydHlTY29wZXNGZWRlcmF0aW9uRW5hYmxlZCI6dHJ1ZX0sImZpbGVzX3NoYXJpbmciOnsic2hhcmVieW1haWwiOnsiZW5hYmxlZCI6dHJ1ZSwidXBsb2FkX2ZpbGVzX2Ryb3AiOnsiZW5hYmxlZCI6dHJ1ZX0sInBhc3N3b3JkIjp7ImVuYWJsZWQiOnRydWUsImVuZm9yY2VkIjpmYWxzZX0sImV4cGlyZV9kYXRlIjp7ImVuYWJsZWQiOnRydWV9fSwiYXBpX2VuYWJsZWQiOnRydWUsInB1YmxpYyI6eyJlbmFibGVkIjp0cnVlLCJwYXNzd29yZCI6eyJlbmZvcmNlZCI6ZmFsc2UsImFza0Zvck9wdGlvbmFsUGFzc3dvcmQiOmZhbHNlfSwiZXhwaXJlX2RhdGUiOnsiZW5hYmxlZCI6ZmFsc2V9LCJtdWx0aXBsZV9saW5rcyI6dHJ1ZSwiZXhwaXJlX2RhdGVfaW50ZXJuYWwiOnsiZW5hYmxlZCI6ZmFsc2V9LCJzZW5kX21haWwiOmZhbHNlLCJ1cGxvYWQiOnRydWUsInVwbG9hZF9maWxlc19kcm9wIjp0cnVlfSwicmVzaGFyaW5nIjp0cnVlLCJ1c2VyIjp7InNlbmRfbWFpbCI6ZmFsc2UsImV4cGlyZV9kYXRlIjp7ImVuYWJsZWQiOnRydWV9fSwiZ3JvdXBfc2hhcmluZyI6dHJ1ZSwiZ3JvdXAiOnsiZW5hYmxlZCI6dHJ1ZSwiZXhwaXJlX2RhdGUiOnsiZW5hYmxlZCI6dHJ1ZX19LCJkZWZhdWx0X3Blcm1pc3Npb25zIjozMSwiZmVkZXJhdGlvbiI6eyJvdXRnb2luZyI6dHJ1ZSwiaW5jb21pbmciOnRydWUsImV4cGlyZV9kYXRlIjp7ImVuYWJsZWQiOnRydWV9fSwic2hhcmVlIjp7InF1ZXJ5X2xvb2t1cF9kZWZhdWx0IjpmYWxzZSwiYWx3YXlzX3Nob3dfdW5pcXVlIjp0cnVlfX0sInRoZW1pbmciOnsibmFtZSI6Ik5leHRjbG91ZCIsInVybCI6Imh0dHBzOlwvXC9uZXh0Y2xvdWQuY29tIiwic2xvZ2FuIjoiYSBzYWZlIGhvbWUgZm9yIGFsbCB5b3VyIGRhdGEiLCJjb2xvciI6IiMwMDgyYzkiLCJjb2xvci10ZXh0IjoiI2ZmZmZmZiIsImNvbG9yLWVsZW1lbnQiOiIjMDA4MmM5IiwiY29sb3ItZWxlbWVudC1icmlnaHQiOiIjMDA4MmM5IiwiY29sb3ItZWxlbWVudC1kYXJrIjoiIzAwODJjOSIsImxvZ28iOiJodHRwOlwvXC93ZWIudHAyLmNlc2lcL2NvcmVcL2ltZ1wvbG9nb1wvbG9nby5zdmc/dj0wIiwiYmFja2dyb3VuZCI6Imh0dHA6XC9cL3dlYi50cDIuY2VzaVwvY29yZVwvaW1nXC9iYWNrZ3JvdW5kLnBuZz92PTAiLCJiYWNrZ3JvdW5kLXBsYWluIjpmYWxzZSwiYmFja2dyb3VuZC1kZWZhdWx0Ijp0cnVlLCJsb2dvaGVhZGVyIjoiaHR0cDpcL1wvd2ViLnRwMi5jZXNpXC9jb3JlXC9pbWdcL2xvZ29cL2xvZ28uc3ZnP3Y9MCIsImZhdmljb24iOiJodHRwOlwvXC93ZWIudHAyLmNlc2lcL2NvcmVcL2ltZ1wvbG9nb1wvbG9nby5zdmc/dj0wIn0sInVzZXJfc3RhdHVzIjp7ImVuYWJsZWQiOnRydWUsInN1cHBvcnRzX2Vtb2ppIjp0cnVlfSwid2VhdGhlcl9zdGF0dXMiOnsiZW5hYmxlZCI6dHJ1ZX19">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                         <input type="hidden" id="initial-state-theming-data" value="eyJuYW1lIjoiTmV4dGNsb3VkIiwidXJsIjoiaHR0cHM6XC9cL25leHRjbG91ZC5jb20iLCJzbG9nYW4iOiJhIHNhZmUgaG9tZSBmb3IgYWxsIHlvdXIgZGF0YSIsImNvbG9yIjoiIzAwODJjOSIsImltcHJpbnRVcmwiOiIiLCJwcml2YWN5VXJsIjoiIiwiaW52ZXJ0ZWQiOmZhbHNlLCJjYWNoZUJ1c3RlciI6IjAifQ==">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                         <input type="hidden" id="initial-state-accessibility-data" value="eyJ0aGVtZSI6ZmFsc2UsImhpZ2hjb250cmFzdCI6ZmFsc2V9">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                 <div class="wrapper">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                         <div class="v-align">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                                                         <header role="banner">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                                 <div id="header">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                                         <div class="logo">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                                                 <h1 class="hidden-visually">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                                                         Nextcloud                                                               </h1>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                                                                                                         </div>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                                 </div>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                         </header>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                                                 <main>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$
[yoann@node1 certs]$ <div id="login"></div>
-bash: syntax error near unexpected token `<'
[yoann@node1 certs]$
[yoann@node1 certs]$                                 </main>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                         </div>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                 </div>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                 <footer role="contentinfo">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                         <p class="info">
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                                 <a href="https://nextcloud.com" target="_blank" rel="noreferrer noopener" class="entity-name">Nextcloud</a> – a safe home for all your data                     </p>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$                 </footer>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$         </body>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$ </html>
-bash: syntax error near unexpected token `newline'
[yoann@node1 certs]$ [yoann@node1 certs]$
-bash: [yoann@node1: command not found
```

```bash
[yoann@proxy ~]$ sudo ls /etc/pki/tls/private/
[sudo] password for yoann:
web.tp2.cesi.key
```

```bash
[yoann@proxy conf.d]$ sudo cat nextcloud-cesi.conf
[sudo] password for yoann:
server{
    # Port listen
    listen 443 ssl http2;
    # SSL
    ssl_certificate         "/etc/pki/tls/certs/web.tp2.cesi.crt";
    ssl_certificate_key     "/etc/pki/tls/private/web.tp2.cesi.key";


    # DNS Record for catch request
    server_name web.tp2.cesi;

    # Open block for process forward request
    location /{
        # Server where traffic is transferred
        proxy_pass http://10.2.1.11;
    }
}
```

```Powershell
PS C:\Windows\system32> curl web.tp2.cesi
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system. If you can read this page, it means that the software it
working correctly.
Just visiting?
This website you are visiting is either experiencing problems or could be going through maintenance.
If you would like the let the administrators of this website know that you've seen this page instead of the page you've expected, you should send them an email. In general, mail
sent to the name "webmaster" and directed to the website's domain should reach the appropriate person.
The most common email address to send to is: "webmaster@example.com"
Note:
The Rocky Linux distribution is a stable and reproduceable platform based on the sources of Red Hat Enterprise Linux (RHEL). With this in mind, please understand that:
Neither the Rocky Linux Project nor the Rocky Enterprise Software Foundation have anything to do with this website or its content.
The Rocky Linux Project nor the RESF have "hacked" this webserver: This test page is included with the distribution.
For more information about Rocky Linux, please visit the Rocky Linux website.
I am the admin, what do I do?
You may now add content to the webroot directory for your software.
For systems using the Apache Webserver: You can add content to the directory /var/www/html/. Until you do so, people visiting your website will see this page. If you would like
this page to not be shown, follow the instructions in: /etc/httpd/conf.d/welcome.conf.
For systems using Nginx: You can add your content in a location of your choice and edit the root configuration directive in /etc/nginx/nginx.conf.
Apache™ is a registered trademark of the Apache Software Foundation in the United States and/or other countries.
NGINX™ is a registered trademark of F5 Networks, Inc..
```