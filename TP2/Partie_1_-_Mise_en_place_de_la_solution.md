# Partie 1 : Mise en place de la solution

## Sommaire :

[[_TOC_]]

# I. Setup base de données

## 1. Installer MariaDB

```bash
[yoann@db ~]$ sudo dnf list | grep mariadb
mariadb.x86_64                                         3:10.3.28-1.module+el8.4.0+427+adf35707           @appstream
mariadb-backup.x86_64                                  3:10.3.28-1.module+el8.4.0+427+adf35707           @appstream
mariadb-common.x86_64                                  3:10.3.28-1.module+el8.4.0+427+adf35707           @appstream
mariadb-connector-c.x86_64                             3.1.11-2.el8_3                                    @appstream
mariadb-connector-c-config.noarch                      3.1.11-2.el8_3                                    @appstream
mariadb-errmsg.x86_64                                  3:10.3.28-1.module+el8.4.0+427+adf35707           @appstream
mariadb-gssapi-server.x86_64                           3:10.3.28-1.module+el8.4.0+427+adf35707           @appstream
mariadb-server.x86_64                                  3:10.3.28-1.module+el8.4.0+427+adf35707           @appstream
mariadb-server-utils.x86_64                            3:10.3.28-1.module+el8.4.0+427+adf35707           @appstream
mariadb-connector-c.i686                               3.1.11-2.el8_3                                    appstream
mariadb-connector-c-devel.i686                         3.1.11-2.el8_3                                    appstream
mariadb-connector-c-devel.x86_64                       3.1.11-2.el8_3                                    appstream
mariadb-connector-odbc.x86_64                          3.1.12-1.el8                                      appstream
mariadb-devel.x86_64                                   3:10.3.28-1.module+el8.4.0+427+adf35707           appstream
mariadb-embedded.x86_64                                3:10.3.28-1.module+el8.4.0+427+adf35707           appstream
mariadb-embedded-devel.x86_64                          3:10.3.28-1.module+el8.4.0+427+adf35707           appstream
mariadb-java-client.noarch                             2.2.5-3.el8                                       appstream
mariadb-oqgraph-engine.x86_64                          3:10.3.28-1.module+el8.4.0+427+adf35707           appstream
mariadb-server-galera.x86_64                           3:10.3.28-1.module+el8.4.0+427+adf35707           appstream
mariadb-test.x86_64                                    3:10.3.28-1.module+el8.4.0+427+adf35707           appstream
```

```bash
[yoann@db ~]$ sudo systemctl start mariadb
[yoann@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
[yoann@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: d>
   Active: active (running) since Tue 2021-12-14 09:40:57 CET; 19s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
 Main PID: 5000 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 23520)
   Memory: 81.1M
   CGroup: /system.slice/mariadb.service
           └─5000 /usr/libexec/mysqld --basedir=/usr

Dec 14 09:40:56 db.tp2.cesi mysql-prepare-db-dir[4897]: See the MariaDB Knowledgebas>
Dec 14 09:40:56 db.tp2.cesi mysql-prepare-db-dir[4897]: MySQL manual for more instru>
Dec 14 09:40:56 db.tp2.cesi mysql-prepare-db-dir[4897]: Please report any problems a>
Dec 14 09:40:56 db.tp2.cesi mysql-prepare-db-dir[4897]: The latest information about>
Dec 14 09:40:56 db.tp2.cesi mysql-prepare-db-dir[4897]: You can find additional info>
Dec 14 09:40:56 db.tp2.cesi mysql-prepare-db-dir[4897]: http://dev.mysql.com
Dec 14 09:40:56 db.tp2.cesi mysql-prepare-db-dir[4897]: Consider joining MariaDB's s>
Dec 14 09:40:56 db.tp2.cesi mysql-prepare-db-dir[4897]: https://mariadb.org/get-invo>
Dec 14 09:40:56 db.tp2.cesi mysqld[5000]: 2021-12-14  9:40:56 0 [Note] /usr/libexec/>
Dec 14 09:40:57 db.tp2.cesi systemd[1]: Started MariaDB 10.3 database server.
```

```bash
[yoann@db ~]$ sudo ss -lutpn | grep mysql
tcp   LISTEN 0      80                 *:3306            *:*    users:(("mysqld",pid=5000,fd=21))
```

```bash
root           1       0  0 09:25 ?        00:00:01 /usr/lib/systemd/systemd --switched-root --system --deserialize 17
```

```bash
mysql       5000       1  0 09:40 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr
```

```bash
[yoann@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[yoann@db ~]$ sudo firewall-cmd --reload
success
```

## 2. Conf MariaDB

```bash
[yoann@db ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

```bash
[yoann@db ~]$ sudo mysql -u root -p
[sudo] password for yoann:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 66100
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```

```bash
CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY '****';
```

```bash 
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```

```bash
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';
```

```bash
FLUSH PRIVILEGES;
```

## 3. Test

```bash
[yoann@db ~]$ sudo dnf provides mysql
[sudo] password for yoann:
Last metadata expiration check: 0:06:54 ago on Tue 14 Dec 2021 10:11:06 AM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared
                                                  : libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

[yoann@db ~]$ sudo dns install mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
sudo: dns: command not found
[yoann@db ~]$ sudo dnf install mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
Last metadata expiration check: 0:07:18 ago on Tue 14 Dec 2021 10:11:06 AM CET.
Dependencies resolved.
====================================================================================
 Package                Arch   Version                              Repo       Size
====================================================================================
Installing:
 mysql                  x86_64 8.0.26-1.module+el8.4.0+652+6de068a7 appstream  12 M
Installing dependencies:
 mariadb-connector-c-config
                        noarch 3.1.11-2.el8_3                       appstream  14 k
 mysql-common           x86_64 8.0.26-1.module+el8.4.0+652+6de068a7 appstream 133 k
Enabling module streams:
 mysql                         8.0

Transaction Summary
====================================================================================
Install  3 Packages

Total download size: 12 M
Installed size: 63 M
Is this ok [y/N]: y
Downloading Packages:
(1/3): mariadb-connector-c-config-3.1.11-2.el8_3.no 136 kB/s |  14 kB     00:00
(2/3): mysql-common-8.0.26-1.module+el8.4.0+652+6de 671 kB/s | 133 kB     00:00
(3/3): mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x  15 MB/s |  12 MB     00:00
------------------------------------------------------------------------------------
Total                                                12 MB/s |  12 MB     00:01
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                            1/1
  Installing       : mariadb-connector-c-config-3.1.11-2.el8_3.noarch           1/3
  Installing       : mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64   2/3
  Installing       : mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64          3/3
  Running scriptlet: mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64          3/3
  Verifying        : mariadb-connector-c-config-3.1.11-2.el8_3.noarch           1/3
  Verifying        : mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64          2/3
  Verifying        : mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64   3/3

Installed:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch
  mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Complete!
```

```bash
[yoann@web ~]$ sudo mysql -u nextcloud -h 10.2.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 67799
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> use nextcloud
Database changed
mysql> show tables;
Empty set (0.00 sec)
```

# II. Setup Apache

## 1. Install Apache

### A. Apache

```bash
[yoann@web ~]$ sudo dnf list | grep httpd
httpd.x86_64                                           2.4.37-43.module+el8.5.0+714+5ec56ee8             @appstream
httpd-filesystem.noarch                                2.4.37-43.module+el8.5.0+714+5ec56ee8             @appstream
httpd-tools.x86_64                                     2.4.37-43.module+el8.5.0+714+5ec56ee8             @appstream
rocky-logos-httpd.noarch                               85.0-3.el8                                        @baseos
httpd-devel.x86_64                                     2.4.37-43.module+el8.5.0+714+5ec56ee8             appstream
httpd-manual.noarch                                    2.4.37-43.module+el8.5.0+714+5ec56ee8             appstream
keycloak-httpd-client-install.noarch                   1.0-2.el8                                         appstream
libmicrohttpd.i686                                     1:0.9.59-2.el8                                    baseos
libmicrohttpd.x86_64                                   1:0.9.59-2.el8                                    baseos
python3-keycloak-httpd-client-install.noarch           1.0-2.el8                                         appstream
```

```bash
[yoann@web ~]$ sudo systemctl start httpd
[yoann@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[yoann@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
 Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: d>
 Active: active (running) since Tue 2021-12-14 10:36:52 CET; 21s ago
 Docs: man:httpd.service(8)
 Main PID: 6813 (httpd)
 Status: "Running, listening on: port 80"
 Tasks: 213 (limit: 23520)
 Memory: 43.3M
 CGroup: /system.slice/httpd.service
 ├─6813 /usr/sbin/httpd -DFOREGROUND
 ├─6814 /usr/sbin/httpd -DFOREGROUND
 ├─6815 /usr/sbin/httpd -DFOREGROUND
 ├─6816 /usr/sbin/httpd -DFOREGROUND
 └─6817 /usr/sbin/httpd -DFOREGROUND
Dec 14 10:36:52 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 14 10:36:52 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 14 10:36:52 web.tp2.cesi httpd[6813]: Server configured, listening on: port 80
[yoann@web ~]$ systemctl list-unit-files | grep enabled | grep httpd
httpd.service                              enabled
[yoann@web ~]$
```

```bash
[yoann@web ~]$ sudo ss -lutpn | grep httpd
tcp   LISTEN 0      128                *:80              *:*    users:(("httpd",pid=6817,fd=4),("httpd",pid=6816,fd=4),("httpd",pid=6815,fd=4),("httpd",pid=6813,fd=4))
```

```bash
[yoann@web ~]$ ps -ef | grep httpd
root         889       1  0 10:14 ?        00:00:02 /usr/sbin/httpd -DFOREGROUND
apache       909     889  0 10:14 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       910     889  0 10:14 ?        00:00:09 /usr/sbin/httpd -DFOREGROUND
apache       912     889  0 10:14 ?        00:00:09 /usr/sbin/httpd -DFOREGROUND
apache       913     889  0 10:14 ?        00:00:11 /usr/sbin/httpd -DFOREGROUND
apache      2626     889  0 10:20 ?        00:01:23 /usr/sbin/httpd -DFOREGROUND
yoann       5217    5176  0 22:10 pts/0    00:00:00 grep --color=auto httpd
```

```bash
root           1       0  0 10:14 ?        00:00:04 /usr/lib/systemd/systemd --switched-root --system --deserialize 16
```

```bash
PS C:\Windows\system32> curl 10.2.1.11
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system. If you can read this page, it means that the software it
working correctly.
Just visiting?
This website you are visiting is either experiencing problems or could be going through maintenance.
If you would like the let the administrators of this website know that you've seen this page instead of the page you've expected, you should send them an email. In general, mail
sent to the name "webmaster" and directed to the website's domain should reach the appropriate person.
The most common email address to send to is: "webmaster@example.com"
Note:
The Rocky Linux distribution is a stable and reproduceable platform based on the sources of Red Hat Enterprise Linux (RHEL). With this in mind, please understand that:
Neither the Rocky Linux Project nor the Rocky Enterprise Software Foundation have anything to do with this website or its content.
The Rocky Linux Project nor the RESF have "hacked" this webserver: This test page is included with the distribution.
For more information about Rocky Linux, please visit the Rocky Linux website.
I am the admin, what do I do?
You may now add content to the webroot directory for your software.
For systems using the Apache Webserver: You can add content to the directory /var/www/html/. Until you do so, people visiting your website will see this page. If you would like
this page to not be shown, follow the instructions in: /etc/httpd/conf.d/welcome.conf.
For systems using Nginx: You can add your content in a location of your choice and edit the root configuration directive in /etc/nginx/nginx.conf.

Apache™ is a registered trademark of the Apache Software Foundation in the United States and/or other countries.
NGINX™ is a registered trademark of F5 Networks, Inc..
```

### B. PHP

```bash
[yoann@web ~]$ sudo dnf install epel-release
[sudo] password for yoann:
Last metadata expiration check: 3:39:54 ago on Fri 17 Dec 2021 07:04:39 PM CET.
Package epel-release-8-13.el8.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

```bash 
sudo dnf update
```

```bash
[yoann@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Last metadata expiration check: 0:01:17 ago on Fri 17 Dec 2021 10:50:09 PM CET.
remi-release-8.rpm                                                                      5.0 kB/s |  26 kB     00:05
Package remi-release-8.5-2.el8.remi.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

```bash
[yoann@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp -y
Last metadata expiration check: 0:00:22 ago on Tue 14 Dec 2021 10:45:16 AM CET.
Package zip-3.0-23.el8.x86_64 is already installed.
Package unzip-6.0-45.el8_4.x86_64 is already installed.
Package libxml2-2.9.7-11.el8.x86_64 is already installed.
Package openssl-1:1.1.1k-4.el8.x86_64 is already installed.
Dependencies resolved.
====================================================================================
 Package                   Arch   Version                           Repo       Size
====================================================================================
Installing:
 php74-php                 x86_64 7.4.26-1.el8.remi                 remi-safe 1.5 M
 php74-php-bcmath          x86_64 7.4.26-1.el8.remi                 remi-safe  88 k
 php74-php-common          x86_64 7.4.26-1.el8.remi                 remi-safe 710 k
 php74-php-gd              x86_64 7.4.26-1.el8.remi                 remi-safe  93 k
 php74-php-gmp             x86_64 7.4.26-1.el8.remi                 remi-safe  84 k
 php74-php-intl            x86_64 7.4.26-1.el8.remi                 remi-safe 201 k
 php74-php-json            x86_64 7.4.26-1.el8.remi                 remi-safe  82 k
 php74-php-mbstring        x86_64 7.4.26-1.el8.remi                 remi-safe 492 k
 php74-php-mysqlnd         x86_64 7.4.26-1.el8.remi                 remi-safe 200 k
 php74-php-pdo             x86_64 7.4.26-1.el8.remi                 remi-safe 130 k
 php74-php-pecl-zip        x86_64 1.20.0-1.el8.remi                 remi-safe  58 k
 php74-php-process         x86_64 7.4.26-1.el8.remi                 remi-safe  92 k
 php74-php-xml             x86_64 7.4.26-1.el8.remi                 remi-safe 180 k
Installing dependencies:
 checkpolicy               x86_64 2.9-1.el8                         baseos    345 k
 environment-modules       x86_64 4.5.2-1.el8                       baseos    420 k
 fontconfig                x86_64 2.13.1-4.el8                      baseos    273 k
 gd                        x86_64 2.2.5-7.el8                       appstream 143 k
 jbigkit-libs              x86_64 2.1-14.el8                        appstream  54 k
 libX11                    x86_64 1.6.8-5.el8                       appstream 610 k
 libX11-common             noarch 1.6.8-5.el8                       appstream 157 k
 libXau                    x86_64 1.0.9-3.el8                       appstream  36 k
 libXpm                    x86_64 3.5.12-8.el8                      appstream  57 k
 libicu69                  x86_64 69.1-1.el8.remi                   remi-safe 9.6 M
 libjpeg-turbo             x86_64 1.5.3-12.el8                      appstream 156 k
 libsodium                 x86_64 1.0.18-2.el8                      epel      162 k
 libtiff                   x86_64 4.0.9-20.el8                      appstream 187 k
 libwebp                   x86_64 1.0.0-5.el8                       appstream 271 k
 libxcb                    x86_64 1.13.1-1.el8                      appstream 228 k
 libxslt                   x86_64 1.1.32-6.el8                      baseos    249 k
 oniguruma5php             x86_64 6.9.7.1-1.el8.remi                remi-safe 210 k
 php74-libzip              x86_64 1.8.0-1.el8.remi                  remi-safe  69 k
 php74-runtime             x86_64 1.0-3.el8.remi                    remi-safe 1.1 M
 policycoreutils-python-utils
 noarch 2.9-16.el8                        baseos    251 k
 python3-audit             x86_64 3.0-0.17.20191104git1c2f876.el8.1 baseos     85 k
 python3-libsemanage       x86_64 2.9-6.el8                         baseos    126 k
 python3-policycoreutils   noarch 2.9-16.el8                        baseos    2.2 M
 python3-setools           x86_64 4.3.0-2.el8                       baseos    625 k
 scl-utils                 x86_64 1:2.0.2-14.el8                    appstream  46 k
 tcl                       x86_64 1:8.6.8-2.el8                     baseos    1.1 M
Installing weak dependencies:
 php74-php-cli             x86_64 7.4.26-1.el8.remi                 remi-safe 3.1 M
 php74-php-fpm             x86_64 7.4.26-1.el8.remi                 remi-safe 1.6 M
 php74-php-opcache         x86_64 7.4.26-1.el8.remi                 remi-safe 275 k
 php74-php-sodium          x86_64 7.4.26-1.el8.remi                 remi-safe  87 k
Transaction Summary
====================================================================================
Install  43 Packages
Total download size: 28 M
Installed size: 91 M
Downloading Packages:
(1/43): jbigkit-libs-2.1-14.el8.x86_64.rpm          363 kB/s |  54 kB     00:00
(2/43): gd-2.2.5-7.el8.x86_64.rpm                   788 kB/s | 143 kB     00:00
(3/43): libXau-1.0.9-3.el8.x86_64.rpm               867 kB/s |  36 kB     00:00
(4/43): libX11-1.6.8-5.el8.x86_64.rpm               2.2 MB/s | 610 kB     00:00
(5/43): libXpm-3.5.12-8.el8.x86_64.rpm              1.0 MB/s |  57 kB     00:00
(6/43): libX11-common-1.6.8-5.el8.noarch.rpm        1.1 MB/s | 157 kB     00:00
(7/43): libjpeg-turbo-1.5.3-12.el8.x86_64.rpm       2.7 MB/s | 156 kB     00:00
(8/43): libtiff-4.0.9-20.el8.x86_64.rpm             2.1 MB/s | 187 kB     00:00
(9/43): libxcb-1.13.1-1.el8.x86_64.rpm              4.0 MB/s | 228 kB     00:00
(10/43): libwebp-1.0.0-5.el8.x86_64.rpm             2.3 MB/s | 271 kB     00:00
(11/43): scl-utils-2.0.2-14.el8.x86_64.rpm          698 kB/s |  46 kB     00:00
(12/43): checkpolicy-2.9-1.el8.x86_64.rpm           1.4 MB/s | 345 kB     00:00
(13/43): fontconfig-2.13.1-4.el8.x86_64.rpm         1.3 MB/s | 273 kB     00:00
(14/43): environment-modules-4.5.2-1.el8.x86_64.rpm 1.5 MB/s | 420 kB     00:00
(15/43): libxslt-1.1.32-6.el8.x86_64.rpm            2.3 MB/s | 249 kB     00:00
(16/43): policycoreutils-python-utils-2.9-16.el8.no 2.0 MB/s | 251 kB     00:00
(17/43): python3-audit-3.0-0.17.20191104git1c2f876. 709 kB/s |  85 kB     00:00
(18/43): python3-libsemanage-2.9-6.el8.x86_64.rpm   1.5 MB/s | 126 kB     00:00
(19/43): python3-setools-4.3.0-2.el8.x86_64.rpm     2.6 MB/s | 625 kB     00:00
(20/43): libsodium-1.0.18-2.el8.x86_64.rpm          1.8 MB/s | 162 kB     00:00
(21/43): tcl-8.6.8-2.el8.x86_64.rpm                 3.1 MB/s | 1.1 MB     00:00
(22/43): python3-policycoreutils-2.9-16.el8.noarch. 4.9 MB/s | 2.2 MB     00:00
(23/43): php74-libzip-1.8.0-1.el8.remi.x86_64.rpm   597 kB/s |  69 kB     00:00
(24/43): oniguruma5php-6.9.7.1-1.el8.remi.x86_64.rp 1.2 MB/s | 210 kB     00:00
(25/43): php74-php-bcmath-7.4.26-1.el8.remi.x86_64. 2.1 MB/s |  88 kB     00:00
(26/43): php74-php-7.4.26-1.el8.remi.x86_64.rpm     5.2 MB/s | 1.5 MB     00:00
(27/43): libicu69-69.1-1.el8.remi.x86_64.rpm         16 MB/s | 9.6 MB     00:00
(28/43): php74-php-common-7.4.26-1.el8.remi.x86_64. 5.3 MB/s | 710 kB     00:00
(29/43): php74-php-gd-7.4.26-1.el8.remi.x86_64.rpm  2.3 MB/s |  93 kB     00:00
(30/43): php74-php-cli-7.4.26-1.el8.remi.x86_64.rpm 7.4 MB/s | 3.1 MB     00:00
(31/43): php74-php-gmp-7.4.26-1.el8.remi.x86_64.rpm 2.2 MB/s |  84 kB     00:00
(32/43): php74-php-intl-7.4.26-1.el8.remi.x86_64.rp 4.1 MB/s | 201 kB     00:00
(33/43): php74-php-json-7.4.26-1.el8.remi.x86_64.rp 2.2 MB/s |  82 kB     00:00
(34/43): php74-php-fpm-7.4.26-1.el8.remi.x86_64.rpm  10 MB/s | 1.6 MB     00:00
(35/43): php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64 4.0 MB/s | 200 kB     00:00
(36/43): php74-php-mbstring-7.4.26-1.el8.remi.x86_6 6.6 MB/s | 492 kB     00:00
(37/43): php74-php-opcache-7.4.26-1.el8.remi.x86_64 5.3 MB/s | 275 kB     00:00
(38/43): php74-php-pdo-7.4.26-1.el8.remi.x86_64.rpm 2.8 MB/s | 130 kB     00:00
(39/43): php74-php-process-7.4.26-1.el8.remi.x86_64 2.4 MB/s |  92 kB     00:00
(40/43): php74-php-pecl-zip-1.20.0-1.el8.remi.x86_6 1.3 MB/s |  58 kB     00:00
(41/43): php74-php-sodium-7.4.26-1.el8.remi.x86_64. 2.2 MB/s |  87 kB     00:00
(42/43): php74-php-xml-7.4.26-1.el8.remi.x86_64.rpm 3.8 MB/s | 180 kB     00:00
(43/43): php74-runtime-1.0-3.el8.remi.x86_64.rpm     14 MB/s | 1.1 MB     00:00
------------------------------------------------------------------------------------
Total                                               7.6 MB/s |  28 MB     00:03
Extra Packages for Enterprise Linux 8 - x86_64      1.6 MB/s | 1.6 kB     00:00
Importing GPG key 0x2F86D6A1:
 Userid     : "Fedora EPEL (8) <epel@fedoraproject.org>"
 Fingerprint: 94E2 79EB 8D8F 25B2 1810 ADF1 21EA 45AB 2F86 D6A1
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-8
Key imported successfully
Safe Remi's RPM repository for Enterprise Linux 8 - 3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
 Preparing        :                                                            1/1
 Installing       : libjpeg-turbo-1.5.3-12.el8.x86_64                         1/43
 Installing       : oniguruma5php-6.9.7.1-1.el8.remi.x86_64                   2/43
 Installing       : libicu69-69.1-1.el8.remi.x86_64                           3/43
 Installing       : libsodium-1.0.18-2.el8.x86_64                             4/43
 Installing       : tcl-1:8.6.8-2.el8.x86_64                                  5/43
 Running scriptlet: tcl-1:8.6.8-2.el8.x86_64                                  5/43
 Installing       : environment-modules-4.5.2-1.el8.x86_64                    6/43
 Running scriptlet: environment-modules-4.5.2-1.el8.x86_64                    6/43
 Installing       : scl-utils-1:2.0.2-14.el8.x86_64                           7/43
 Installing       : python3-setools-4.3.0-2.el8.x86_64                        8/43
 Installing       : python3-libsemanage-2.9-6.el8.x86_64                      9/43
 Installing       : python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64   10/43
 Installing       : libxslt-1.1.32-6.el8.x86_64                              11/43
 Installing       : fontconfig-2.13.1-4.el8.x86_64                           12/43
 Running scriptlet: fontconfig-2.13.1-4.el8.x86_64                           12/43
 Installing       : checkpolicy-2.9-1.el8.x86_64                             13/43
 Installing       : python3-policycoreutils-2.9-16.el8.noarch                14/43
 Installing       : policycoreutils-python-utils-2.9-16.el8.noarch           15/43
 Installing       : php74-runtime-1.0-3.el8.remi.x86_64                      16/43
 Running scriptlet: php74-runtime-1.0-3.el8.remi.x86_64                      16/43
 Installing       : php74-php-json-7.4.26-1.el8.remi.x86_64                  17/43
 Installing       : php74-php-common-7.4.26-1.el8.remi.x86_64                18/43
 Installing       : php74-php-pdo-7.4.26-1.el8.remi.x86_64                   19/43
 Installing       : php74-php-cli-7.4.26-1.el8.remi.x86_64                   20/43
 Installing       : php74-php-fpm-7.4.26-1.el8.remi.x86_64                   21/43
 Running scriptlet: php74-php-fpm-7.4.26-1.el8.remi.x86_64                   21/43
 Installing       : php74-php-mbstring-7.4.26-1.el8.remi.x86_64              22/43
 Installing       : php74-php-opcache-7.4.26-1.el8.remi.x86_64               23/43
 Installing       : php74-php-sodium-7.4.26-1.el8.remi.x86_64                24/43
 Installing       : php74-php-xml-7.4.26-1.el8.remi.x86_64                   25/43
 Installing       : php74-libzip-1.8.0-1.el8.remi.x86_64                     26/43
 Installing       : libwebp-1.0.0-5.el8.x86_64                               27/43
 Installing       : libXau-1.0.9-3.el8.x86_64                                28/43
 Installing       : libxcb-1.13.1-1.el8.x86_64                               29/43
 Installing       : libX11-common-1.6.8-5.el8.noarch                         30/43
 Installing       : libX11-1.6.8-5.el8.x86_64                                31/43
 Installing       : libXpm-3.5.12-8.el8.x86_64                               32/43
 Installing       : jbigkit-libs-2.1-14.el8.x86_64                           33/43
 Running scriptlet: jbigkit-libs-2.1-14.el8.x86_64                           33/43
 Installing       : libtiff-4.0.9-20.el8.x86_64                              34/43
 Installing       : gd-2.2.5-7.el8.x86_64                                    35/43
 Running scriptlet: gd-2.2.5-7.el8.x86_64                                    35/43
 Installing       : php74-php-gd-7.4.26-1.el8.remi.x86_64                    36/43
 Installing       : php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64              37/43
 Installing       : php74-php-7.4.26-1.el8.remi.x86_64                       38/43
 Installing       : php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64               39/43
 Installing       : php74-php-bcmath-7.4.26-1.el8.remi.x86_64                40/43
 Installing       : php74-php-gmp-7.4.26-1.el8.remi.x86_64                   41/43
 Installing       : php74-php-intl-7.4.26-1.el8.remi.x86_64                  42/43
 Installing       : php74-php-process-7.4.26-1.el8.remi.x86_64               43/43
 Running scriptlet: php74-php-process-7.4.26-1.el8.remi.x86_64               43/43
 Running scriptlet: fontconfig-2.13.1-4.el8.x86_64                           43/43
 Running scriptlet: php74-php-fpm-7.4.26-1.el8.remi.x86_64                   43/43
 Verifying        : gd-2.2.5-7.el8.x86_64                                     1/43
 Verifying        : jbigkit-libs-2.1-14.el8.x86_64                            2/43
 Verifying        : libX11-1.6.8-5.el8.x86_64                                 3/43
 Verifying        : libX11-common-1.6.8-5.el8.noarch                          4/43
 Verifying        : libXau-1.0.9-3.el8.x86_64                                 5/43
 Verifying        : libXpm-3.5.12-8.el8.x86_64                                6/43
 Verifying        : libjpeg-turbo-1.5.3-12.el8.x86_64                         7/43
 Verifying        : libtiff-4.0.9-20.el8.x86_64                               8/43
 Verifying        : libwebp-1.0.0-5.el8.x86_64                                9/43
 Verifying        : libxcb-1.13.1-1.el8.x86_64                               10/43
 Verifying        : scl-utils-1:2.0.2-14.el8.x86_64                          11/43
 Verifying        : checkpolicy-2.9-1.el8.x86_64                             12/43
 Verifying        : environment-modules-4.5.2-1.el8.x86_64                   13/43
 Verifying        : fontconfig-2.13.1-4.el8.x86_64                           14/43
 Verifying        : libxslt-1.1.32-6.el8.x86_64                              15/43
 Verifying        : policycoreutils-python-utils-2.9-16.el8.noarch           16/43
 Verifying        : python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64   17/43
 Verifying        : python3-libsemanage-2.9-6.el8.x86_64                     18/43
 Verifying        : python3-policycoreutils-2.9-16.el8.noarch                19/43
 Verifying        : python3-setools-4.3.0-2.el8.x86_64                       20/43
 Verifying        : tcl-1:8.6.8-2.el8.x86_64                                 21/43
 Verifying        : libsodium-1.0.18-2.el8.x86_64                            22/43
 Verifying        : libicu69-69.1-1.el8.remi.x86_64                          23/43
 Verifying        : oniguruma5php-6.9.7.1-1.el8.remi.x86_64                  24/43
 Verifying        : php74-libzip-1.8.0-1.el8.remi.x86_64                     25/43
 Verifying        : php74-php-7.4.26-1.el8.remi.x86_64                       26/43
 Verifying        : php74-php-bcmath-7.4.26-1.el8.remi.x86_64                27/43
 Verifying        : php74-php-cli-7.4.26-1.el8.remi.x86_64                   28/43
 Verifying        : php74-php-common-7.4.26-1.el8.remi.x86_64                29/43
 Verifying        : php74-php-fpm-7.4.26-1.el8.remi.x86_64                   30/43
 Verifying        : php74-php-gd-7.4.26-1.el8.remi.x86_64                    31/43
 Verifying        : php74-php-gmp-7.4.26-1.el8.remi.x86_64                   32/43
 Verifying        : php74-php-intl-7.4.26-1.el8.remi.x86_64                  33/43
 Verifying        : php74-php-json-7.4.26-1.el8.remi.x86_64                  34/43
 Verifying        : php74-php-mbstring-7.4.26-1.el8.remi.x86_64              35/43
 Verifying        : php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64               36/43
 Verifying        : php74-php-opcache-7.4.26-1.el8.remi.x86_64               37/43
 Verifying        : php74-php-pdo-7.4.26-1.el8.remi.x86_64                   38/43
 Verifying        : php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64              39/43
 Verifying        : php74-php-process-7.4.26-1.el8.remi.x86_64               40/43
 Verifying        : php74-php-sodium-7.4.26-1.el8.remi.x86_64                41/43
 Verifying        : php74-php-xml-7.4.26-1.el8.remi.x86_64                   42/43
 Verifying        : php74-runtime-1.0-3.el8.remi.x86_64                      43/43
Installed:
 checkpolicy-2.9-1.el8.x86_64
 environment-modules-4.5.2-1.el8.x86_64
 fontconfig-2.13.1-4.el8.x86_64
 gd-2.2.5-7.el8.x86_64
 jbigkit-libs-2.1-14.el8.x86_64
 libX11-1.6.8-5.el8.x86_64
 libX11-common-1.6.8-5.el8.noarch
 libXau-1.0.9-3.el8.x86_64
 libXpm-3.5.12-8.el8.x86_64
 libicu69-69.1-1.el8.remi.x86_64
 libjpeg-turbo-1.5.3-12.el8.x86_64
 libsodium-1.0.18-2.el8.x86_64
 libtiff-4.0.9-20.el8.x86_64
 libwebp-1.0.0-5.el8.x86_64
 libxcb-1.13.1-1.el8.x86_64
 libxslt-1.1.32-6.el8.x86_64
 oniguruma5php-6.9.7.1-1.el8.remi.x86_64
 php74-libzip-1.8.0-1.el8.remi.x86_64
 php74-php-7.4.26-1.el8.remi.x86_64
 php74-php-bcmath-7.4.26-1.el8.remi.x86_64
 php74-php-cli-7.4.26-1.el8.remi.x86_64
 php74-php-common-7.4.26-1.el8.remi.x86_64
 php74-php-fpm-7.4.26-1.el8.remi.x86_64
 php74-php-gd-7.4.26-1.el8.remi.x86_64
 php74-php-gmp-7.4.26-1.el8.remi.x86_64
 php74-php-intl-7.4.26-1.el8.remi.x86_64
 php74-php-json-7.4.26-1.el8.remi.x86_64
 php74-php-mbstring-7.4.26-1.el8.remi.x86_64
 php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64
 php74-php-opcache-7.4.26-1.el8.remi.x86_64
 php74-php-pdo-7.4.26-1.el8.remi.x86_64
 php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64
 php74-php-process-7.4.26-1.el8.remi.x86_64
 php74-php-sodium-7.4.26-1.el8.remi.x86_64
 php74-php-xml-7.4.26-1.el8.remi.x86_64
 php74-runtime-1.0-3.el8.remi.x86_64
 policycoreutils-python-utils-2.9-16.el8.noarch
 python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64
 python3-libsemanage-2.9-6.el8.x86_64
 python3-policycoreutils-2.9-16.el8.noarch
 python3-setools-4.3.0-2.el8.x86_64
 scl-utils-1:2.0.2-14.el8.x86_64
 tcl-1:8.6.8-2.el8.x86_64
Complete!
```

```bash
IncludeOptional conf.d/*.conf
```

```bash
[yoann@web conf.d]$ sudo cat cesi.conf
[sudo] password for yoann:
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

```bash
[yoann@web www]$ sudo chown apache nextcloud -R
[yoann@web www]$ ls -la
total 4
drwxr-xr-x.  5 root   root   50 Dec 14 14:14 .
drwxr-xr-x. 22 root   root 4096 Dec 14 10:35 ..
drwxr-xr-x.  2 root   root    6 Nov 15 04:13 cgi-bin
drwxr-xr-x.  2 root   root    6 Nov 15 04:13 html
drwxr-xr-x.  3 apache root   18 Dec 14 14:14 nextcloud
[yoann@web www]$ cd nextcloud/
[yoann@web nextcloud]$ ls -la
total 0
drwxr-xr-x. 3 apache root 18 Dec 14 14:14 .
drwxr-xr-x. 5 root   root 50 Dec 14 14:14 ..
drwxr-xr-x. 2 apache root  6 Dec 14 14:14 html
[yoann@web nextcloud]$
```

```bash
[yoann@web conf.d]$ cd
[yoann@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
 % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  36.3M      0  0:00:04  0:00:04 --:--:-- 36.3M
[yoann@web ~]$ ls
curl  nextcloud-21.0.1.zip
```

```bash
[yoann@web ~]$ sudo un
zip  nextcloud-21.0.1.zip
[yoann@web ~]$ sudo mv nextcloud /var/www/nextcloud/
[yoann@web ~]$ sudo chown -R apache /var/www/nextcloud
```

```bash
# Copyright (c) 1993-2009 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host

# localhost name resolution is handled within DNS itself.
#	127.0.0.1       localhost
#	::1             localhost
10.2.1.11       web.tp2.cesi
```

```Powershell
PS C:\Windows\system32> curl web.tp2.cesi
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system. If you can read this page, it means that the software it
working correctly.
Just visiting?
This website you are visiting is either experiencing problems or could be going through maintenance.
If you would like the let the administrators of this website know that you've seen this page instead of the page you've expected, you should send them an email. In general, mail
sent to the name "webmaster" and directed to the website's domain should reach the appropriate person.
The most common email address to send to is: "webmaster@example.com"
Note:
The Rocky Linux distribution is a stable and reproduceable platform based on the sources of Red Hat Enterprise Linux (RHEL). With this in mind, please understand that:
Neither the Rocky Linux Project nor the Rocky Enterprise Software Foundation have anything to do with this website or its content.
The Rocky Linux Project nor the RESF have "hacked" this webserver: This test page is included with the distribution.
For more information about Rocky Linux, please visit the Rocky Linux website.
I am the admin, what do I do?
You may now add content to the webroot directory for your software.
For systems using the Apache Webserver: You can add content to the directory /var/www/html/. Until you do so, people visiting your website will see this page. If you would like
this page to not be shown, follow the instructions in: /etc/httpd/conf.d/welcome.conf.
For systems using Nginx: You can add your content in a location of your choice and edit the root configuration directive in /etc/nginx/nginx.conf.
Apache™ is a registered trademark of the Apache Software Foundation in the United States and/or other countries.
NGINX™ is a registered trademark of F5 Networks, Inc..
```