# Partie 3 : Maintien en condition opérationnelle

## Sommaire :
[[_TOC_]]

# I. Monitoring

# 2. Setup Netdata

```bash
[yoann@web ~]$ sudo systemctl enable netdata
[yoann@web ~]$ sudo systemctl start netdata
[yoann@web ~]$ systemctl status netdata
● netdata.service - Real time performance monitoring
 Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset:>
 Active: active (running) since Wed 2021-12-15 10:56:24 CET; 1min 12s ago
 Main PID: 5428 (netdata)
 Tasks: 37 (limit: 23520)
 Memory: 49.4M
 CGroup: /system.slice/netdata.service
 ├─5428 /opt/netdata/bin/srv/netdata -P /opt/netdata/var/run/netdata/netd>
 ├─5449 /opt/netdata/bin/srv/netdata --special-spawn-server
 ├─5605 /opt/netdata/usr/libexec/netdata/plugins.d/go.d.plugin 1
 └─5631 /opt/netdata/usr/libexec/netdata/plugins.d/apps.plugin 1
Dec 15 10:56:24 web.tp2.cesi netdata[5428]: 2021-12-15 10:56:24: netdata INFO  : MA>
Dec 15 10:56:24 web.tp2.cesi [5428]: Found 0 legacy dbengines, setting multidb disk>
Dec 15 10:56:24 web.tp2.cesi netdata[5428]: 2021-12-15 10:56:24: netdata INFO  : MA>
Dec 15 10:56:24 web.tp2.cesi [5428]: Created file '/opt/netdata/var/lib/netdata/dbe>
Dec 15 10:56:24 web.tp2.cesi netdata[5428]: 2021-12-15 10:56:24: netdata INFO  : MA>
Dec 15 10:56:25 web.tp2.cesi [5616]: Does not have a configuration file inside `/op>
Dec 15 10:56:25 web.tp2.cesi [5616]: Name resolution is disabled, collector will no>
Dec 15 10:56:25 web.tp2.cesi [5616]: The network value of CIDR 127.0.0.1/8 was upda>
Dec 15 10:56:25 web.tp2.cesi [5616]: PROCFILE: Cannot open file '/opt/netdata/etc/n>
Dec 15 10:56:25 web.tp2.cesi [5616]: Cannot read process groups configuration file >
lines 1-22/22 (END)
```

```bash
[yoann@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[yoann@web ~]$ sudo firewall-cmd --reload
success
```

# II. Backup

```bash
[yoann@web conf.d]$ cd
[yoann@web ~]$ curl -SLO https://github.com/borgbackup/borg/releases/download/1.1.17/borg-linux64
 % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
 Dload  Upload   Total   Spent    Left  Speed
100   649  100   649    0     0   1996      0 --:--:-- --:--:-- --:--:--  1996
100 18.0M  100 18.0M    0     0  13.5M      0  0:00:01  0:00:01 --:--:-- 30.7M
[yoann@web ~]$ sudo cp borg-linux64 /usr/local/bin/borg
[sudo] password for yoann:
[yoann@web ~]$ sudo chown root:root /usr/local/bin/borg
[yoann@web ~]$ sudo chmod 755 /usr/local/bin/borg
```

```bash
[root@web ~]# var=$(printf 'nextcloud_%(%Y%m%d_%H%M%S)T')
[root@web ~]# echo $var
nextcloud_20211215_114542
[root@web ~]# borg create /srv/backup::$var /var/www/nextcloud/
Enter passphrase for key /srv/backup:
[root@web ~]# borg list /srv/backup/
Enter passphrase for key /srv/backup:
Enter passphrase for key /srv/backup:
test Wed, 2021-12-15 11:35:20 [9a6c717d63782bc2b7207d4aa455697c862b3e4ec0d22a875bbbb38d5c915035]
nextcloud_20211215_114542            Wed, 2021-12-15 11:47:20 [f9351dbe1adea9be889ce6352b1d0d2b65dbc8874717ba032374fd7387066c26]
```

```bash
export BORG_PASSPHRASE=passphrase
var=$(printf 'nextcloud_%(%Y%m%d_%H%M%S)T')
/usr/local/bin/borg create /srv/backup::$var /var/www/nextcloud/
```

```bash
[yoann@web ~]$ sudo cat /etc/systemd/system/backup_db.service
[Unit]
Description=<DESCRIPTION>
[Service]
ExecStart=bash /etc/scripts/borg_save_nextcloud.sh
Type=oneshot
[Install]
WantedBy=multi-user.target
```

```bash
[yoann@web ~]$ sudo cat /etc/systemd/system/backup.timer
[Unit]
Description=Lance backup.service à intervalles réguliers
Requires=backup_db.service
[Timer]
Unit=backup_db.service
OnCalendar=hourly
[Install]
WantedBy=timers.target
```

```bash
[yoann@web ~]$ sudo systemctl daemon-reload
[yoann@web ~]$ sudo systemctl start backup.timer
[yoann@web ~]$ sudo systemctl enable backup.timer
Created symlink /etc/systemd/system/timers.target.wants/backup.timer → /etc/systemd/                                                                                                                          system/backup.timer.
[yoann@web ~]$ sudo systemctl list-timers
NEXT                         LEFT       LAST                         PASSED       U>
Wed 2021-12-15 15:00:00 CET  39min left n/a                          n/a          b>
Wed 2021-12-15 15:09:23 CET  49min left Wed 2021-12-15 13:53:25 CET  26min ago    d>
Thu 2021-12-16 00:00:00 CET  9h left    Wed 2021-12-15 03:40:00 CET  10h ago      m>
Thu 2021-12-16 08:49:49 CET  18h left   Wed 2021-12-15 08:49:49 CET  5h 30min ago s>
4 timers listed.
```