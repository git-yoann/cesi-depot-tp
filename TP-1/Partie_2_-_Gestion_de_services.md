# II. Gestion de services

## Sommaire

[[_TOC_]]

## 1. Analyse du service SSH

```bash
systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2021-12-15 14:31:10 CET; 6h ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 864 (sshd)
    Tasks: 1 (limit: 4943)
   Memory: 3.9M
   CGroup: /system.slice/sshd.service
           └─864 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cbc>

Dec 15 19:21:46 node1.tp1.cesi sshd[1867]: Accepted publickey for yoann from 10.2.1.1 port 1079 ssh2: RSA SHA256:7KgnjA>
Dec 15 19:21:46 node1.tp1.cesi sshd[1867]: pam_unix(sshd:session): session opened for user yoann by (uid=0)
Dec 15 19:29:34 node1.tp1.cesi sshd[1922]: Accepted password for root from 10.2.1.1 port 1269 ssh2
Dec 15 19:29:34 node1.tp1.cesi sshd[1922]: pam_unix(sshd:session): session opened for user root by (uid=0)
Dec 15 20:57:28 node1.tp1.cesi sshd[2049]: Accepted publickey for yoann from 10.2.1.1 port 12720 ssh2: RSA SHA256:7Kgnj>
Dec 15 20:57:28 node1.tp1.cesi sshd[2049]: pam_unix(sshd:session): session opened for user yoann by (uid=0)
Dec 15 21:09:01 node1.tp1.cesi sshd[2140]: Accepted password for root from 10.2.1.1 port 12887 ssh2
Dec 15 21:09:01 node1.tp1.cesi sshd[2140]: pam_unix(sshd:session): session opened for user root by (uid=0)
Dec 15 21:10:40 node1.tp1.cesi sshd[2189]: Accepted publickey for yoann from 10.2.1.1 port 12907 ssh2: RSA SHA256:7Kgnj>
Dec 15 21:10:40 node1.tp1.cesi sshd[2189]: pam_unix(sshd:session): session opened for user yoann by (uid=0)
```

```bash
[yoann@node1 ~]$ sudo ss -lutpn | grep ssh
[sudo] password for yoann:
tcp   LISTEN 0      128          0.0.0.0:22        0.0.0.0:*    users:(("sshd",pid=864,fd=5))
tcp   LISTEN 0      128             [::]:22           [::]:*    users:(("sshd",pid=864,fd=7))
```

## 2. Modification du service SSH

```bash
[yoann@node1 ~]$ ps -ef | grep ssh
root         864       1  0 14:31 ?        00:00:00 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cbc,aes128-gcm@openssh.com,aes128-ctr,aes128-cbc -oMACs=hmac-sha2-256-etm@openssh.com,hmac-sha1-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512-etm@openssh.com,hmac-sha2-256,hmac-sha1,umac-128@openssh.com,hmac-sha2-512 -oGSSAPIKexAlgorithms=gss-curve25519-sha256-,gss-nistp256-sha256-,gss-group14-sha256-,gss-group16-sha512-,gss-gex-sha1-,gss-group14-sha1- -oKexAlgorithms=curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,diffie-hellman-group14-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group-exchange-sha1,diffie-hellman-group14-sha1 -oHostKeyAlgorithms=ecdsa-sha2-nistp256,ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521,ecdsa-sha2-nistp521-cert-v01@openssh.com,ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-256,rsa-sha2-256-cert-v01@openssh.com,rsa-sha2-512,rsa-sha2-512-cert-v01@openssh.com,ssh-rsa,ssh-rsa-cert-v01@openssh.com -oPubkeyAcceptedKeyTypes=ecdsa-sha2-nistp256,ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521,ecdsa-sha2-nistp521-cert-v01@openssh.com,ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-256,rsa-sha2-256-cert-v01@openssh.com,rsa-sha2-512,rsa-sha2-512-cert-v01@openssh.com,ssh-rsa,ssh-rsa-cert-v01@openssh.com -oCASignatureAlgorithms=ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-ed25519,rsa-sha2-256,rsa-sha2-512,ssh-rsa
root        2189     864  0 21:10 ?        00:00:00 sshd: yoann [priv]
yoann       2203    2189  0 21:10 ?        00:00:00 sshd: yoann@pts/0
yoann       2267    2204  0 21:32 pts/0    00:00:00 grep --color=auto ssh
```

```bash
[yoann@node1 ~]$ sudo firewall-cmd --permanent --add-port=20/tcp
success
[yoann@node1 ~]$ sudo vi /etc/ssh/sshd_config
[yoann@node1 ~]$ sudo cat /etc/ssh/sshd_config

#       $OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
Port 20
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::

HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# This system is following system-wide crypto policy. The changes to
# crypto properties (Ciphers, MACs, ...) will not have any effect here.
# They will be overridden by command-line options passed to the server
# on command line.
# Please, check manual pages for update-crypto-policies(8) and sshd_config(5).

# Logging
#SyslogFacility AUTH
SyslogFacility AUTHPRIV
#LogLevel INFO

# Authentication:

#LoginGraceTime 2m
PermitRootLogin yes
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

#PubkeyAuthentication yes

# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile      .ssh/authorized_keys

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication yes
#PermitEmptyPasswords no
PasswordAuthentication yes

# Change to no to disable s/key passwords
#ChallengeResponseAuthentication yes
ChallengeResponseAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no
#KerberosUseKuserok yes

# GSSAPI options
GSSAPIAuthentication yes
GSSAPICleanupCredentials no
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no
#GSSAPIEnablek5users no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
# WARNING: 'UsePAM no' is not supported in Fedora and may cause several
# problems.
UsePAM yes

#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
X11Forwarding yes
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes

# It is recommended to use pam_motd in /etc/pam.d/sshd instead of PrintMotd,
# as it is more configurable and versatile than the built-in version.
PrintMotd no

#PrintLastLog yes
#TCPKeepAlive yes
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
#UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# Accept locale-related environment variables
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS

# override default of no subsystems
Subsystem       sftp    /usr/libexec/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#       X11Forwarding no
#       AllowTcpForwarding no
#       PermitTTY no
#       ForceCommand cvs server
```

```bash
[yoann@node1 ~]$ sudo firewall-cmd --reload
success
[yoann@node1 ~]$ systemctl restart sshd
```

```bash
[yoann@node1 ~]$ sudo ss -lutpn | grep ssh
[sudo] password for yoann:
tcp   LISTEN 0      128          0.0.0.0:20        0.0.0.0:*    users:(("sshd",pid=868,fd=5))
tcp   LISTEN 0      128             [::]:20           [::]:*    users:(("sshd",pid=868,fd=7))
```

```bash 
PS C:\WINDOWS\system32> ssh yoann@10.2.1.20 -p 20

Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Dec 15 21:47:07 2021 from 10.2.1.1
```

```bash
[yoann@node1 ~]$ sudo dnf install nginx
[sudo] password for yoann:
Last metadata expiration check: 1:01:11 ago on Mon 13 Dec 2021 12:56:22 PM CET.
Dependencies resolved.
====================================================================================
 Package              Arch   Version                                Repo       Size
====================================================================================
Installing:
 nginx                x86_64 1:1.14.1-9.module+el8.4.0+542+81547229 appstream 566 k
Installing dependencies:
 fontconfig           x86_64 2.13.1-4.el8                           baseos    273 k
 gd                   x86_64 2.2.5-7.el8                            appstream 143 k
 jbigkit-libs         x86_64 2.1-14.el8                             appstream  54 k
 libX11               x86_64 1.6.8-5.el8                            appstream 610 k
 libX11-common        noarch 1.6.8-5.el8                            appstream 157 k
 libXau               x86_64 1.0.9-3.el8                            appstream  36 k
 libXpm               x86_64 3.5.12-8.el8                           appstream  57 k
 libjpeg-turbo        x86_64 1.5.3-12.el8                           appstream 156 k
 libtiff              x86_64 4.0.9-20.el8                           appstream 187 k
 libwebp              x86_64 1.0.0-5.el8                            appstream 271 k
 libxcb               x86_64 1.13.1-1.el8                           appstream 228 k
 libxslt              x86_64 1.1.32-6.el8                           baseos    249 k
 nginx-all-modules    noarch 1:1.14.1-9.module+el8.4.0+542+81547229 appstream  22 k
 nginx-filesystem     noarch 1:1.14.1-9.module+el8.4.0+542+81547229 appstream  23 k
 nginx-mod-http-image-filter
 x86_64 1:1.14.1-9.module+el8.4.0+542+81547229 appstream  34 k
 nginx-mod-http-perl  x86_64 1:1.14.1-9.module+el8.4.0+542+81547229 appstream  45 k
 nginx-mod-http-xslt-filter
 x86_64 1:1.14.1-9.module+el8.4.0+542+81547229 appstream  32 k
 nginx-mod-mail       x86_64 1:1.14.1-9.module+el8.4.0+542+81547229 appstream  63 k
 nginx-mod-stream     x86_64 1:1.14.1-9.module+el8.4.0+542+81547229 appstream  84 k
 perl-Carp            noarch 1.42-396.el8                           baseos     29 k
 perl-Data-Dumper     x86_64 2.167-399.el8                          baseos     57 k
 perl-Digest          noarch 1.17-395.el8                           appstream  26 k
 perl-Digest-MD5      x86_64 2.55-396.el8                           appstream  36 k
 perl-Encode          x86_64 4:2.97-3.el8                           baseos    1.5 M
 perl-Errno           x86_64 1.28-420.el8                           baseos     75 k
 perl-Exporter        noarch 5.72-396.el8                           baseos     33 k
 perl-File-Path       noarch 2.15-2.el8                             baseos     37 k
 perl-File-Temp       noarch 0.230.600-1.el8                        baseos     62 k
 perl-Getopt-Long     noarch 1:2.50-4.el8                           baseos     62 k
 perl-HTTP-Tiny       noarch 0.074-1.el8                            baseos     57 k
 perl-IO              x86_64 1.38-420.el8                           baseos    141 k
 perl-MIME-Base64     x86_64 3.15-396.el8                           baseos     30 k
 perl-Net-SSLeay      x86_64 1.88-1.module+el8.4.0+512+d4f0fc54     appstream 378 k
 perl-PathTools       x86_64 3.74-1.el8                             baseos     89 k
 perl-Pod-Escapes     noarch 1:1.07-395.el8                         baseos     19 k
 perl-Pod-Perldoc     noarch 3.28-396.el8                           baseos     85 k
 perl-Pod-Simple      noarch 1:3.35-395.el8                         baseos    212 k
 perl-Pod-Usage       noarch 4:1.69-395.el8                         baseos     33 k
 perl-Scalar-List-Utils
 x86_64 3:1.49-2.el8                           baseos     67 k
 perl-Socket          x86_64 4:2.027-3.el8                          baseos     58 k
 perl-Storable        x86_64 1:3.11-3.el8                           baseos     97 k
 perl-Term-ANSIColor  noarch 4.06-396.el8                           baseos     45 k
 perl-Term-Cap        noarch 1.17-395.el8                           baseos     22 k
 perl-Text-ParseWords noarch 3.30-395.el8                           baseos     17 k
 perl-Text-Tabs+Wrap  noarch 2013.0523-395.el8                      baseos     23 k
 perl-Time-Local      noarch 1:1.280-1.el8                          baseos     32 k
 perl-URI             noarch 1.73-3.el8                             appstream 115 k
 perl-Unicode-Normalize
 x86_64 1.25-396.el8                           baseos     81 k
 perl-constant        noarch 1.33-396.el8                           baseos     24 k
 perl-interpreter     x86_64 4:5.26.3-420.el8                       baseos    6.3 M
 perl-libnet          noarch 3.11-3.el8                             appstream 120 k
 perl-libs            x86_64 4:5.26.3-420.el8                       baseos    1.6 M
 perl-macros          x86_64 4:5.26.3-420.el8                       baseos     71 k
 perl-parent          noarch 1:0.237-1.el8                          baseos     19 k
 perl-podlators       noarch 4.11-1.el8                             baseos    117 k
 perl-threads         x86_64 1:2.21-2.el8                           baseos     60 k
 perl-threads-shared  x86_64 1.58-2.el8                             baseos     47 k
Installing weak dependencies:
 perl-IO-Socket-IP    noarch 0.39-5.el8                             appstream  46 k
 perl-IO-Socket-SSL   noarch 2.066-4.module+el8.4.0+512+d4f0fc54    appstream 297 k
 perl-Mozilla-CA      noarch 20160104-7.module+el8.4.0+529+e3b3e624 appstream  14 k
Enabling module streams:
 nginx                       1.14
 perl                        5.26
 perl-IO-Socket-SSL          2.066
 perl-libwww-perl            6.34
Transaction Summary
====================================================================================
Install  61 Packages
Total download size: 15 M
Installed size: 45 M
Is this ok [y/N]: y
Downloading Packages:
(1/61): jbigkit-libs-2.1-14.el8.x86_64.rpm          354 kB/s |  54 kB     00:00
(2/61): gd-2.2.5-7.el8.x86_64.rpm                   701 kB/s | 143 kB     00:00
(3/61): libXau-1.0.9-3.el8.x86_64.rpm               905 kB/s |  36 kB     00:00
(4/61): libXpm-3.5.12-8.el8.x86_64.rpm              1.2 MB/s |  57 kB     00:00
(5/61): libX11-1.6.8-5.el8.x86_64.rpm               2.0 MB/s | 610 kB     00:00
(6/61): libX11-common-1.6.8-5.el8.noarch.rpm        1.1 MB/s | 157 kB     00:00
(7/61): libtiff-4.0.9-20.el8.x86_64.rpm             3.3 MB/s | 187 kB     00:00
(8/61): libjpeg-turbo-1.5.3-12.el8.x86_64.rpm       2.0 MB/s | 156 kB     00:00
(9/61): libwebp-1.0.0-5.el8.x86_64.rpm              2.7 MB/s | 271 kB     00:00
(10/61): libxcb-1.13.1-1.el8.x86_64.rpm             4.0 MB/s | 228 kB     00:00
(11/61): nginx-filesystem-1.14.1-9.module+el8.4.0+5 761 kB/s |  23 kB     00:00
(12/61): nginx-all-modules-1.14.1-9.module+el8.4.0+ 526 kB/s |  22 kB     00:00
(13/61): nginx-mod-http-image-filter-1.14.1-9.modul 758 kB/s |  34 kB     00:00
(14/61): nginx-mod-http-perl-1.14.1-9.module+el8.4. 889 kB/s |  45 kB     00:00
(15/61): nginx-mod-http-xslt-filter-1.14.1-9.module 1.0 MB/s |  32 kB     00:00
(16/61): nginx-1.14.1-9.module+el8.4.0+542+81547229 3.5 MB/s | 566 kB     00:00
(17/61): nginx-mod-mail-1.14.1-9.module+el8.4.0+542 1.6 MB/s |  63 kB     00:00
(18/61): nginx-mod-stream-1.14.1-9.module+el8.4.0+5 2.2 MB/s |  84 kB     00:00
(19/61): perl-Digest-1.17-395.el8.noarch.rpm        463 kB/s |  26 kB     00:00
(20/61): perl-Digest-MD5-2.55-396.el8.x86_64.rpm    538 kB/s |  36 kB     00:00
(21/61): perl-Mozilla-CA-20160104-7.module+el8.4.0+ 358 kB/s |  14 kB     00:00
(22/61): perl-IO-Socket-IP-0.39-5.el8.noarch.rpm    533 kB/s |  46 kB     00:00
(23/61): perl-IO-Socket-SSL-2.066-4.module+el8.4.0+ 2.5 MB/s | 297 kB     00:00
(24/61): perl-URI-1.73-3.el8.noarch.rpm             884 kB/s | 115 kB     00:00
(25/61): perl-Net-SSLeay-1.88-1.module+el8.4.0+512+ 2.7 MB/s | 378 kB     00:00
(26/61): perl-libnet-3.11-3.el8.noarch.rpm          1.4 MB/s | 120 kB     00:00
(27/61): perl-Carp-1.42-396.el8.noarch.rpm          283 kB/s |  29 kB     00:00
(28/61): perl-Data-Dumper-2.167-399.el8.x86_64.rpm  760 kB/s |  57 kB     00:00
(29/61): libxslt-1.1.32-6.el8.x86_64.rpm            1.2 MB/s | 249 kB     00:00
(30/61): fontconfig-2.13.1-4.el8.x86_64.rpm         1.1 MB/s | 273 kB     00:00
(31/61): perl-Errno-1.28-420.el8.x86_64.rpm         1.3 MB/s |  75 kB     00:00
(32/61): perl-Exporter-5.72-396.el8.noarch.rpm      472 kB/s |  33 kB     00:00
(33/61): perl-File-Path-2.15-2.el8.noarch.rpm       325 kB/s |  37 kB     00:00
(34/61): perl-File-Temp-0.230.600-1.el8.noarch.rpm  428 kB/s |  62 kB     00:00
(35/61): perl-Getopt-Long-2.50-4.el8.noarch.rpm     764 kB/s |  62 kB     00:00
(36/61): perl-HTTP-Tiny-0.074-1.el8.noarch.rpm      814 kB/s |  57 kB     00:00
(37/61): perl-IO-1.38-420.el8.x86_64.rpm            1.9 MB/s | 141 kB     00:00
(38/61): perl-MIME-Base64-3.15-396.el8.x86_64.rpm   535 kB/s |  30 kB     00:00
(39/61): perl-Encode-2.97-3.el8.x86_64.rpm          3.8 MB/s | 1.5 MB     00:00
(40/61): perl-PathTools-3.74-1.el8.x86_64.rpm       1.2 MB/s |  89 kB     00:00
(41/61): perl-Pod-Escapes-1.07-395.el8.noarch.rpm   366 kB/s |  19 kB     00:00
(42/61): perl-Pod-Perldoc-3.28-396.el8.noarch.rpm   1.1 MB/s |  85 kB     00:00
(43/61): perl-Scalar-List-Utils-1.49-2.el8.x86_64.r 962 kB/s |  67 kB     00:00
(44/61): perl-Pod-Simple-3.35-395.el8.noarch.rpm    1.7 MB/s | 212 kB     00:00
(45/61): perl-Pod-Usage-1.69-395.el8.noarch.rpm     326 kB/s |  33 kB     00:00
(46/61): perl-Socket-2.027-3.el8.x86_64.rpm         949 kB/s |  58 kB     00:00
(47/61): perl-Storable-3.11-3.el8.x86_64.rpm        1.5 MB/s |  97 kB     00:00
(48/61): perl-Term-ANSIColor-4.06-396.el8.noarch.rp 659 kB/s |  45 kB     00:00
(49/61): perl-Term-Cap-1.17-395.el8.noarch.rpm      518 kB/s |  22 kB     00:00
(50/61): perl-Text-ParseWords-3.30-395.el8.noarch.r 310 kB/s |  17 kB     00:00
(51/61): perl-Text-Tabs+Wrap-2013.0523-395.el8.noar 333 kB/s |  23 kB     00:00
(52/61): perl-Time-Local-1.280-1.el8.noarch.rpm     659 kB/s |  32 kB     00:00
(53/61): perl-Unicode-Normalize-1.25-396.el8.x86_64 1.5 MB/s |  81 kB     00:00
(54/61): perl-constant-1.33-396.el8.noarch.rpm      611 kB/s |  24 kB     00:00
(55/61): perl-macros-5.26.3-420.el8.x86_64.rpm      947 kB/s |  71 kB     00:00
(56/61): perl-parent-0.237-1.el8.noarch.rpm         298 kB/s |  19 kB     00:00
(57/61): perl-podlators-4.11-1.el8.noarch.rpm       1.4 MB/s | 117 kB     00:00
(58/61): perl-threads-2.21-2.el8.x86_64.rpm         983 kB/s |  60 kB     00:00
(59/61): perl-libs-5.26.3-420.el8.x86_64.rpm        5.1 MB/s | 1.6 MB     00:00
(60/61): perl-threads-shared-1.58-2.el8.x86_64.rpm  1.1 MB/s |  47 kB     00:00
(61/61): perl-interpreter-5.26.3-420.el8.x86_64.rpm 4.5 MB/s | 6.3 MB     00:01
------------------------------------------------------------------------------------
Total                                               4.4 MB/s |  15 MB     00:03
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
 Preparing        :                                                            1/1
 Installing       : libjpeg-turbo-1.5.3-12.el8.x86_64                         1/61
 Installing       : perl-Digest-1.17-395.el8.noarch                           2/61
 Installing       : perl-Digest-MD5-2.55-396.el8.x86_64                       3/61
 Installing       : perl-Data-Dumper-2.167-399.el8.x86_64                     4/61
 Installing       : perl-libnet-3.11-3.el8.noarch                             5/61
 Installing       : perl-Net-SSLeay-1.88-1.module+el8.4.0+512+d4f0fc54.x86    6/61
 Installing       : perl-URI-1.73-3.el8.noarch                                7/61
 Installing       : perl-Pod-Escapes-1:1.07-395.el8.noarch                    8/61
 Installing       : perl-Mozilla-CA-20160104-7.module+el8.4.0+529+e3b3e624    9/61
 Installing       : perl-IO-Socket-IP-0.39-5.el8.noarch                      10/61
 Installing       : perl-Time-Local-1:1.280-1.el8.noarch                     11/61
 Installing       : perl-IO-Socket-SSL-2.066-4.module+el8.4.0+512+d4f0fc54   12/61
 Installing       : perl-Term-ANSIColor-4.06-396.el8.noarch                  13/61
 Installing       : perl-Term-Cap-1.17-395.el8.noarch                        14/61
 Installing       : perl-File-Temp-0.230.600-1.el8.noarch                    15/61
 Installing       : perl-Pod-Simple-1:3.35-395.el8.noarch                    16/61
 Installing       : perl-HTTP-Tiny-0.074-1.el8.noarch                        17/61
 Installing       : perl-podlators-4.11-1.el8.noarch                         18/61
 Installing       : perl-Pod-Perldoc-3.28-396.el8.noarch                     19/61
 Installing       : perl-Text-ParseWords-3.30-395.el8.noarch                 20/61
 Installing       : perl-Pod-Usage-4:1.69-395.el8.noarch                     21/61
 Installing       : perl-MIME-Base64-3.15-396.el8.x86_64                     22/61
 Installing       : perl-Storable-1:3.11-3.el8.x86_64                        23/61
 Installing       : perl-Getopt-Long-1:2.50-4.el8.noarch                     24/61
 Installing       : perl-Errno-1.28-420.el8.x86_64                           25/61
 Installing       : perl-Socket-4:2.027-3.el8.x86_64                         26/61
 Installing       : perl-Encode-4:2.97-3.el8.x86_64                          27/61
 Installing       : perl-Carp-1.42-396.el8.noarch                            28/61
 Installing       : perl-Exporter-5.72-396.el8.noarch                        29/61
 Installing       : perl-libs-4:5.26.3-420.el8.x86_64                        30/61
 Installing       : perl-Scalar-List-Utils-3:1.49-2.el8.x86_64               31/61
 Installing       : perl-parent-1:0.237-1.el8.noarch                         32/61
 Installing       : perl-macros-4:5.26.3-420.el8.x86_64                      33/61
 Installing       : perl-Text-Tabs+Wrap-2013.0523-395.el8.noarch             34/61
 Installing       : perl-Unicode-Normalize-1.25-396.el8.x86_64               35/61
 Installing       : perl-File-Path-2.15-2.el8.noarch                         36/61
 Installing       : perl-IO-1.38-420.el8.x86_64                              37/61
 Installing       : perl-PathTools-3.74-1.el8.x86_64                         38/61
 Installing       : perl-constant-1.33-396.el8.noarch                        39/61
 Installing       : perl-threads-1:2.21-2.el8.x86_64                         40/61
 Installing       : perl-threads-shared-1.58-2.el8.x86_64                    41/61
 Installing       : perl-interpreter-4:5.26.3-420.el8.x86_64                 42/61
 Installing       : libxslt-1.1.32-6.el8.x86_64                              43/61
 Installing       : fontconfig-2.13.1-4.el8.x86_64                           44/61
 Running scriptlet: fontconfig-2.13.1-4.el8.x86_64                           44/61
 Running scriptlet: nginx-filesystem-1:1.14.1-9.module+el8.4.0+542+8154722   45/61
 Installing       : nginx-filesystem-1:1.14.1-9.module+el8.4.0+542+8154722   45/61
 Installing       : libwebp-1.0.0-5.el8.x86_64                               46/61
 Installing       : libXau-1.0.9-3.el8.x86_64                                47/61
 Installing       : libxcb-1.13.1-1.el8.x86_64                               48/61
 Installing       : libX11-common-1.6.8-5.el8.noarch                         49/61
 Installing       : libX11-1.6.8-5.el8.x86_64                                50/61
 Installing       : libXpm-3.5.12-8.el8.x86_64                               51/61
 Installing       : jbigkit-libs-2.1-14.el8.x86_64                           52/61
 Running scriptlet: jbigkit-libs-2.1-14.el8.x86_64                           52/61
 Installing       : libtiff-4.0.9-20.el8.x86_64                              53/61
 Installing       : gd-2.2.5-7.el8.x86_64                                    54/61
 Running scriptlet: gd-2.2.5-7.el8.x86_64                                    54/61
 Installing       : nginx-mod-http-perl-1:1.14.1-9.module+el8.4.0+542+8154   55/61
 Running scriptlet: nginx-mod-http-perl-1:1.14.1-9.module+el8.4.0+542+8154   55/61
 Installing       : nginx-mod-http-xslt-filter-1:1.14.1-9.module+el8.4.0+5   56/61
 Running scriptlet: nginx-mod-http-xslt-filter-1:1.14.1-9.module+el8.4.0+5   56/61
 Installing       : nginx-mod-mail-1:1.14.1-9.module+el8.4.0+542+81547229.   57/61
 Running scriptlet: nginx-mod-mail-1:1.14.1-9.module+el8.4.0+542+81547229.   57/61
 Installing       : nginx-mod-stream-1:1.14.1-9.module+el8.4.0+542+8154722   58/61
 Running scriptlet: nginx-mod-stream-1:1.14.1-9.module+el8.4.0+542+8154722   58/61
 Installing       : nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64      59/61
 Running scriptlet: nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64      59/61
 Installing       : nginx-mod-http-image-filter-1:1.14.1-9.module+el8.4.0+   60/61
 Running scriptlet: nginx-mod-http-image-filter-1:1.14.1-9.module+el8.4.0+   60/61
 Installing       : nginx-all-modules-1:1.14.1-9.module+el8.4.0+542+815472   61/61
 Running scriptlet: nginx-all-modules-1:1.14.1-9.module+el8.4.0+542+815472   61/61
 Running scriptlet: fontconfig-2.13.1-4.el8.x86_64                           61/61
 Verifying        : gd-2.2.5-7.el8.x86_64                                     1/61
 Verifying        : jbigkit-libs-2.1-14.el8.x86_64                            2/61
 Verifying        : libX11-1.6.8-5.el8.x86_64                                 3/61
 Verifying        : libX11-common-1.6.8-5.el8.noarch                          4/61
 Verifying        : libXau-1.0.9-3.el8.x86_64                                 5/61
 Verifying        : libXpm-3.5.12-8.el8.x86_64                                6/61
 Verifying        : libjpeg-turbo-1.5.3-12.el8.x86_64                         7/61
 Verifying        : libtiff-4.0.9-20.el8.x86_64                               8/61
 Verifying        : libwebp-1.0.0-5.el8.x86_64                                9/61
 Verifying        : libxcb-1.13.1-1.el8.x86_64                               10/61
 Verifying        : nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64      11/61
 Verifying        : nginx-all-modules-1:1.14.1-9.module+el8.4.0+542+815472   12/61
 Verifying        : nginx-filesystem-1:1.14.1-9.module+el8.4.0+542+8154722   13/61
 Verifying        : nginx-mod-http-image-filter-1:1.14.1-9.module+el8.4.0+   14/61
 Verifying        : nginx-mod-http-perl-1:1.14.1-9.module+el8.4.0+542+8154   15/61
 Verifying        : nginx-mod-http-xslt-filter-1:1.14.1-9.module+el8.4.0+5   16/61
 Verifying        : nginx-mod-mail-1:1.14.1-9.module+el8.4.0+542+81547229.   17/61
 Verifying        : nginx-mod-stream-1:1.14.1-9.module+el8.4.0+542+8154722   18/61
 Verifying        : perl-Digest-1.17-395.el8.noarch                          19/61
 Verifying        : perl-Digest-MD5-2.55-396.el8.x86_64                      20/61
 Verifying        : perl-IO-Socket-IP-0.39-5.el8.noarch                      21/61
 Verifying        : perl-IO-Socket-SSL-2.066-4.module+el8.4.0+512+d4f0fc54   22/61
 Verifying        : perl-Mozilla-CA-20160104-7.module+el8.4.0+529+e3b3e624   23/61
 Verifying        : perl-Net-SSLeay-1.88-1.module+el8.4.0+512+d4f0fc54.x86   24/61
 Verifying        : perl-URI-1.73-3.el8.noarch                               25/61
 Verifying        : perl-libnet-3.11-3.el8.noarch                            26/61
 Verifying        : fontconfig-2.13.1-4.el8.x86_64                           27/61
 Verifying        : libxslt-1.1.32-6.el8.x86_64                              28/61
 Verifying        : perl-Carp-1.42-396.el8.noarch                            29/61
 Verifying        : perl-Data-Dumper-2.167-399.el8.x86_64                    30/61
 Verifying        : perl-Encode-4:2.97-3.el8.x86_64                          31/61
 Verifying        : perl-Errno-1.28-420.el8.x86_64                           32/61
 Verifying        : perl-Exporter-5.72-396.el8.noarch                        33/61
 Verifying        : perl-File-Path-2.15-2.el8.noarch                         34/61
 Verifying        : perl-File-Temp-0.230.600-1.el8.noarch                    35/61
 Verifying        : perl-Getopt-Long-1:2.50-4.el8.noarch                     36/61
 Verifying        : perl-HTTP-Tiny-0.074-1.el8.noarch                        37/61
 Verifying        : perl-IO-1.38-420.el8.x86_64                              38/61
 Verifying        : perl-MIME-Base64-3.15-396.el8.x86_64                     39/61
 Verifying        : perl-PathTools-3.74-1.el8.x86_64                         40/61
 Verifying        : perl-Pod-Escapes-1:1.07-395.el8.noarch                   41/61
 Verifying        : perl-Pod-Perldoc-3.28-396.el8.noarch                     42/61
 Verifying        : perl-Pod-Simple-1:3.35-395.el8.noarch                    43/61
 Verifying        : perl-Pod-Usage-4:1.69-395.el8.noarch                     44/61
 Verifying        : perl-Scalar-List-Utils-3:1.49-2.el8.x86_64               45/61
 Verifying        : perl-Socket-4:2.027-3.el8.x86_64                         46/61
 Verifying        : perl-Storable-1:3.11-3.el8.x86_64                        47/61
 Verifying        : perl-Term-ANSIColor-4.06-396.el8.noarch                  48/61
 Verifying        : perl-Term-Cap-1.17-395.el8.noarch                        49/61
 Verifying        : perl-Text-ParseWords-3.30-395.el8.noarch                 50/61
 Verifying        : perl-Text-Tabs+Wrap-2013.0523-395.el8.noarch             51/61
 Verifying        : perl-Time-Local-1:1.280-1.el8.noarch                     52/61
 Verifying        : perl-Unicode-Normalize-1.25-396.el8.x86_64               53/61
 Verifying        : perl-constant-1.33-396.el8.noarch                        54/61
 Verifying        : perl-interpreter-4:5.26.3-420.el8.x86_64                 55/61
 Verifying        : perl-libs-4:5.26.3-420.el8.x86_64                        56/61
 Verifying        : perl-macros-4:5.26.3-420.el8.x86_64                      57/61
 Verifying        : perl-parent-1:0.237-1.el8.noarch                         58/61
 Verifying        : perl-podlators-4.11-1.el8.noarch                         59/61
 Verifying        : perl-threads-1:2.21-2.el8.x86_64                         60/61
 Verifying        : perl-threads-shared-1.58-2.el8.x86_64                    61/61
Installed:
 fontconfig-2.13.1-4.el8.x86_64
 gd-2.2.5-7.el8.x86_64
 jbigkit-libs-2.1-14.el8.x86_64
 libX11-1.6.8-5.el8.x86_64
 libX11-common-1.6.8-5.el8.noarch
 libXau-1.0.9-3.el8.x86_64
 libXpm-3.5.12-8.el8.x86_64
 libjpeg-turbo-1.5.3-12.el8.x86_64
 libtiff-4.0.9-20.el8.x86_64
 libwebp-1.0.0-5.el8.x86_64
 libxcb-1.13.1-1.el8.x86_64
 libxslt-1.1.32-6.el8.x86_64
 nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
 nginx-all-modules-1:1.14.1-9.module+el8.4.0+542+81547229.noarch
 nginx-filesystem-1:1.14.1-9.module+el8.4.0+542+81547229.noarch
 nginx-mod-http-image-filter-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
 nginx-mod-http-perl-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
 nginx-mod-http-xslt-filter-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
 nginx-mod-mail-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
 nginx-mod-stream-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
 perl-Carp-1.42-396.el8.noarch
 perl-Data-Dumper-2.167-399.el8.x86_64
 perl-Digest-1.17-395.el8.noarch
 perl-Digest-MD5-2.55-396.el8.x86_64
 perl-Encode-4:2.97-3.el8.x86_64
 perl-Errno-1.28-420.el8.x86_64
 perl-Exporter-5.72-396.el8.noarch
 perl-File-Path-2.15-2.el8.noarch
 perl-File-Temp-0.230.600-1.el8.noarch
 perl-Getopt-Long-1:2.50-4.el8.noarch
 perl-HTTP-Tiny-0.074-1.el8.noarch
 perl-IO-1.38-420.el8.x86_64
 perl-IO-Socket-IP-0.39-5.el8.noarch
 perl-IO-Socket-SSL-2.066-4.module+el8.4.0+512+d4f0fc54.noarch
 perl-MIME-Base64-3.15-396.el8.x86_64
 perl-Mozilla-CA-20160104-7.module+el8.4.0+529+e3b3e624.noarch
 perl-Net-SSLeay-1.88-1.module+el8.4.0+512+d4f0fc54.x86_64
 perl-PathTools-3.74-1.el8.x86_64
 perl-Pod-Escapes-1:1.07-395.el8.noarch
 perl-Pod-Perldoc-3.28-396.el8.noarch
 perl-Pod-Simple-1:3.35-395.el8.noarch
 perl-Pod-Usage-4:1.69-395.el8.noarch
 perl-Scalar-List-Utils-3:1.49-2.el8.x86_64
 perl-Socket-4:2.027-3.el8.x86_64
 perl-Storable-1:3.11-3.el8.x86_64
 perl-Term-ANSIColor-4.06-396.el8.noarch
 perl-Term-Cap-1.17-395.el8.noarch
 perl-Text-ParseWords-3.30-395.el8.noarch
 perl-Text-Tabs+Wrap-2013.0523-395.el8.noarch
 perl-Time-Local-1:1.280-1.el8.noarch
 perl-URI-1.73-3.el8.noarch
 perl-Unicode-Normalize-1.25-396.el8.x86_64
 perl-constant-1.33-396.el8.noarch
 perl-interpreter-4:5.26.3-420.el8.x86_64
 perl-libnet-3.11-3.el8.noarch
 perl-libs-4:5.26.3-420.el8.x86_64
 perl-macros-4:5.26.3-420.el8.x86_64
 perl-parent-1:0.237-1.el8.noarch
 perl-podlators-4.11-1.el8.noarch
 perl-threads-1:2.21-2.el8.x86_64
 perl-threads-shared-1.58-2.el8.x86_64
Complete!
```

```bash
[yoann@node1 ~]$ systemctl start nginx.service
```

```bash
[yoann@node1 ~]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: >
   Active: active (running) since Mon 2021-12-13 14:58:46 CET; 1min 16s ago
  Process: 9395 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 9393 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 9391 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/S>
 Main PID: 9396 (nginx)
    Tasks: 5 (limit: 23520)
   Memory: 7.6M
   CGroup: /system.slice/nginx.service
           ├─9396 nginx: master process /usr/sbin/nginx
           ├─9397 nginx: worker process
           ├─9398 nginx: worker process
           ├─9399 nginx: worker process
           └─9400 nginx: worker process

Dec 13 14:58:46 node1.tp1.cesi systemd[1]: Starting The nginx HTTP and reverse prox>
Dec 13 14:58:46 node1.tp1.cesi nginx[9393]: nginx: the configuration file /etc/ngin>
Dec 13 14:58:46 node1.tp1.cesi nginx[9393]: nginx: configuration file /etc/nginx/ng>
Dec 13 14:58:46 node1.tp1.cesi systemd[1]: Started The nginx HTTP and reverse proxy>
lines 1-20/20 (END)
```

```bash
[yoann@node1 ~]$  sudo ss -lutpn | grep nginx
tcp   LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=9400,fd=8),("nginx",pid=9399,fd=8),("nginx",pid=9398,fd=8),("nginx",pid=9397,fd=8),("nginx",pid=9396,fd=8))
tcp   LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=9400,fd=9),("nginx",pid=9399,fd=9),("nginx",pid=9398,fd=9),("nginx",pid=9397,fd=9),("nginx",pid=9396,fd=9))
```

```bash
[yoann@node1 ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
success
[yoann@node1 ~]$ sudo firewall-cmd --reload
success
```

```Powershell
PS C:\Windows\system32> curl 10.2.1.20:80


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

                    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
                      <head>
                        <title>Test Page for the Nginx...
RawContent        : HTTP/1.1 200 OK
                    Connection: keep-alive
                    Accept-Ranges: bytes
                    Content-Length: 3429
                    Content-Type: text/html
                    Date: Mon, 13 Dec 2021 13:04:24 GMT
                    ETag: "60c1d6af-d65"
                    Last-Modified: Thu, 10 Jun 2021...
Forms             : {}
Headers           : {[Connection, keep-alive], [Accept-Ranges, bytes], [Content-Length, 3429], [Content-Type, text/html]...}
Images            : {@{innerHTML=; innerText=; outerHTML=<IMG alt="[ Powered by nginx ]" src="nginx-logo.png" width=121 height=32>;
                    outerText=; tagName=IMG; alt=[ Powered by nginx ]; src=nginx-logo.png; width=121; height=32}, @{innerHTML=;
                    innerText=; outerHTML=<IMG alt="[ Powered by Rocky Linux ]" src="poweredby.png" width=88 height=31>; outerText=;
                    tagName=IMG; alt=[ Powered by Rocky Linux ]; src=poweredby.png; width=88; height=31}}
InputFields       : {}
Links             : {@{innerHTML=Rocky Linux website; innerText=Rocky Linux website; outerHTML=<A
                    href="https://www.rockylinux.org/">Rocky Linux website</A>; outerText=Rocky Linux website; tagName=A;
                    href=https://www.rockylinux.org/}, @{innerHTML=available on the Rocky Linux website; innerText=available on the
                    Rocky Linux website; outerHTML=<A href="https://www.rockylinux.org/">available on the Rocky Linux website</A>;
                    outerText=available on the Rocky Linux website; tagName=A; href=https://www.rockylinux.org/}, @{innerHTML=<IMG
                    alt="[ Powered by nginx ]" src="nginx-logo.png" width=121 height=32>; innerText=; outerHTML=<A
                    href="http://nginx.net/"><IMG alt="[ Powered by nginx ]" src="nginx-logo.png" width=121 height=32></A>;
                    outerText=; tagName=A; href=http://nginx.net/}, @{innerHTML=<IMG alt="[ Powered by Rocky Linux ]"
                    src="poweredby.png" width=88 height=31>; innerText=; outerHTML=<A href="http://www.rockylinux.org/"><IMG alt="[
                    Powered by Rocky Linux ]" src="poweredby.png" width=88 height=31></A>; outerText=; tagName=A;
                    href=http://www.rockylinux.org/}}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 3429
```

```bash
[yoann@node1 ~]$ sudo dnf install python3
Last metadata expiration check: 1:10:28 ago on Mon 13 Dec 2021 12:56:22 PM CET.
Dependencies resolved.
====================================================================================
 Package             Arch    Version                               Repository  Size
====================================================================================
Installing:
 python36            x86_64  3.6.8-38.module+el8.5.0+671+195e4563  appstream   18 k
Installing dependencies:
 python3-pip         noarch  9.0.3-20.el8.rocky.0                  appstream   19 k
 python3-setuptools  noarch  39.2.0-6.el8                          baseos     162 k
Enabling module streams:
 python36                    3.6

Transaction Summary
====================================================================================
Install  3 Packages

Total download size: 199 k
Installed size: 466 k
Is this ok [y/N]: y
Downloading Packages:
(1/3): python3-pip-9.0.3-20.el8.rocky.0.noarch.rpm  172 kB/s |  19 kB     00:00
(2/3): python36-3.6.8-38.module+el8.5.0+671+195e456 161 kB/s |  18 kB     00:00
(3/3): python3-setuptools-39.2.0-6.el8.noarch.rpm   696 kB/s | 162 kB     00:00
------------------------------------------------------------------------------------
Total                                               288 kB/s | 199 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                            1/1
  Installing       : python3-setuptools-39.2.0-6.el8.noarch                     1/3
  Installing       : python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64       2/3
  Running scriptlet: python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64       2/3
  Installing       : python3-pip-9.0.3-20.el8.rocky.0.noarch                    3/3
  Running scriptlet: python3-pip-9.0.3-20.el8.rocky.0.noarch                    3/3
  Verifying        : python3-pip-9.0.3-20.el8.rocky.0.noarch                    1/3
  Verifying        : python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64       2/3
  Verifying        : python3-setuptools-39.2.0-6.el8.noarch                     3/3

Installed:
  python3-pip-9.0.3-20.el8.rocky.0.noarch
  python3-setuptools-39.2.0-6.el8.noarch
  python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64

Complete!
```

```bash
[yoann@node1 ~]$ sudo firewall-cmd --permanent --add-port=8888/tcp
success
[yoann@node1 ~]$ sudo firewall-cmd --reload
success
```

```bash
[yoann@node1 ~]$ python3 -m http.server 8888
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
10.2.1.20 - - [13/Dec/2021 14:09:41] "GET / HTTP/1.1" 200 -
10.2.1.20 - - [13/Dec/2021 14:09:41] code 404, message File not found
10.2.1.20 - - [13/Dec/2021 14:09:41] "GET /favicon.ico HTTP/1.1" 404 -
10.2.1.20 - - [13/Dec/2021 14:09:51] "GET / HTTP/1.1" 200 -
```

```Powershell
PS C:\Windows\system32> curl 10.2.1.20:8888
StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
 <html>
 <head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title>Directory listing fo...
RawContent        : HTTP/1.0 200 OK
 Content-Length: 702
 Content-Type: text/html; charset=utf-8
 Date: Mon, 13 Dec 2021 13:09:51 GMT
 Server: SimpleHTTP/0.6 Python/3.6.8
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//...
Forms             : {}
Headers           : {[Content-Length, 702], [Content-Type, text/html; charset=utf-8], [Date, Mon, 13 Dec 2021 13:09:51 GMT],
 [Server, SimpleHTTP/0.6 Python/3.6.8]}
Images            : {}
InputFields       : {}
Links             : {@{innerHTML=.bash_history; innerText=.bash_history; outerHTML=<A href=".bash_history">.bash_history</A>;
 outerText=.bash_history; tagName=A; href=.bash_history}, @{innerHTML=.bash_logout; innerText=.bash_logout;
 outerHTML=<A href=".bash_logout">.bash_logout</A>; outerText=.bash_logout; tagName=A; href=.bash_logout},
 @{innerHTML=.bash_profile; innerText=.bash_profile; outerHTML=<A href=".bash_profile">.bash_profile</A>;
 outerText=.bash_profile; tagName=A; href=.bash_profile}, @{innerHTML=.bashrc; innerText=.bashrc; outerHTML=<A
 href=".bashrc">.bashrc</A>; outerText=.bashrc; tagName=A; href=.bashrc}...}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 702
Créer le fichier de conf pour le nouveau service : 
[yoann@node1 system]$ cat /etc/systemd/system/test.service
[Unit]
Description=service test
[Service]
ExecStart=/usr/bin/python3 -m http.server 8888
[Install]
WantedBy=multi-user.target
Démarrer le nouveau service :
[yoann@node1 system]$ sudo systemctl daemon-reload
[yoann@node1 system]$ sudo systemctl status test.service
● test.service - service test
 Loaded: loaded (/etc/systemd/system/test.service; disabled; vendor preset: disab>
 Active: inactive (dead)
Dec 13 14:16:06 node1.tp1.cesi systemd[1]: /etc/systemd/system/test.service:1: Inva>
Dec 13 14:16:07 node1.tp1.cesi systemd[1]: /etc/systemd/system/test.service:1: Inva>
[yoann@node1 system]$ sudo systemctl start test.service
[yoann@node1 system]$ systemctl start test.service
[yoann@node1 system]$ sudo systemctl status test.service
● test.service - service test
 Loaded: loaded (/etc/systemd/system/test.service; disabled; vendor preset: disab>
 Active: active (running) since Mon 2021-12-13 14:18:15 CET; 6s ago
 Main PID: 9929 (python3)
 Tasks: 1 (limit: 23520)
 Memory: 9.4M
 CGroup: /system.slice/test.service
 └─9929 /usr/bin/python3 -m http.server 8888
Dec 13 14:18:15 node1.tp1.cesi systemd[1]: Started service test.
```

```bash
[yoann@node1 system]$ sudo systemctl enable test.service
Created symlink /etc/systemd/system/multi-user.target.wants/test.service → /etc/systemd/system/test.service.
[yoann@node1 system]$ systemctl list-unit-files | grep enabled | grep test
test.service                               enabled
```

```Powershell
PS C:\Windows\system32> curl 10.2.1.20:8888
StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
 <html>
 <head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title>Directory listing fo...
RawContent        : HTTP/1.0 200 OK
 Content-Length: 942
 Content-Type: text/html; charset=utf-8
 Date: Mon, 13 Dec 2021 13:21:22 GMT
 Server: SimpleHTTP/0.6 Python/3.6.8
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//...
Forms             : {}
Headers           : {[Content-Length, 942], [Content-Type, text/html; charset=utf-8], [Date, Mon, 13 Dec 2021 13:21:22 GMT],
 [Server, SimpleHTTP/0.6 Python/3.6.8]}
Images            : {}
InputFields       : {}
Links             : {@{innerHTML=bin@; innerText=bin@; outerHTML=<A href="bin/">bin@</A>; outerText=bin@; tagName=A; href=bin/},
 @{innerHTML=boot/; innerText=boot/; outerHTML=<A href="boot/">boot/</A>; outerText=boot/; tagName=A;
 href=boot/}, @{innerHTML=dev/; innerText=dev/; outerHTML=<A href="dev/">dev/</A>; outerText=dev/; tagName=A;
 href=dev/}, @{innerHTML=etc/; innerText=etc/; outerHTML=<A href="etc/">etc/</A>; outerText=etc/; tagName=A;
 href=etc/}...}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 942
```

```bash
Créer USER web :
[yoann@node1 system]$ sudo useradd web
[sudo] password for yoann:
[yoann@node1 system]$ groups web
web : web
```

```bash
[yoann@node1 system]$ sudo mkdir srv
[yoann@node1 system]$ sudo mkdir /srv/web
[yoann@node1 system]$ cd /srv/web
[yoann@node1 web]$ sudo nano test
[yoann@node1 web]$ cat /srv/web/test
I like Linux
```

```bash
[yoann@node1 web]$ sudo chown web /srv/web/test
[yoann@node1 web]$ sudo ls -al /srv/web
total 4
drwxr-xr-x. 2 root root 18 Dec 13 14:34 .
drwxr-xr-x. 3 root root 17 Dec 13 14:34 ..
-rw-r--r--. 1 web  root 26 Dec 13 14:34 test
```

```bash
[yoann@node1 web]$ cat /etc/systemd/system/test.service
[Unit]
Description=service test
[Service]
ExecStart=/usr/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/srv/web/
[Install]
WantedBy=multi-user.target
```

```Powershell
PS C:\Windows\system32> curl 10.2.1.20:8888


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                    <html>
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    <title>Directory listing fo...
RawContent        : HTTP/1.0 200 OK
                    Content-Length: 338
                    Content-Type: text/html; charset=utf-8
                    Date: Mon, 13 Dec 2021 13:53:08 GMT
                    Server: SimpleHTTP/0.6 Python/3.6.8

                    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//...
Forms             : {}
Headers           : {[Content-Length, 338], [Content-Type, text/html; charset=utf-8], [Date, Mon, 13 Dec 2021 13:53:08 GMT],
                    [Server, SimpleHTTP/0.6 Python/3.6.8]}
Images            : {}
InputFields       : {}
Links             : {@{innerHTML=test.txt; innerText=test.txt; outerHTML=<A href="test.txt">test.txt</A>; outerText=test.txt;
                    tagName=A; href=test.txt}}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 338
```