# I. Préparation de la machine

## Sommaire

[[_TOC_]]
 
## 1. Réseau

### A. IP statique

> La commande ```ip a``` liste les interface réseaux

```bash
[root@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:c0:9b:5d brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 82109sec preferred_lft 82109sec
    inet6 fe80::a00:27ff:fec0:9b5d/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:62:82:73 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.20/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe62:8273/64 scope link
       valid_lft forever preferred_lft forever
```

> Pour modifier une interface réseau, il faut se rendre sur le chemin ci dessous

```bash
[root@node1 ~]$ cd /etc/sysconfig/network-scripts/
[root@node1 network-scripts]$ ls
ifcfg-enp0s3  ifcfg-enp0s8
```

```bash
[root@node1 network-scripts]$ vi ifcfg-enp0s8
```

```bash
NAME=enp0s8                 # nom de l'interface
DEVICE=enp0s8               # nom de l'interface
BOOTPROTO=static            # définition statique de l'IP (par opposition à DHCP)
ONBOOT=yes                  # la carte s'allumera automatiquement au boot de la machine
IPADDR=10.1.1.20            # adresse IP choisie
NETMASK=255.255.255.0       # masque choisi au format binaire. Ex : 255.255.255.0
```

>Redemarre l'interface

```bash
[root@node1 network-scripts]$ nmcli con reload enp0s8
[root@node1 network-scripts]$ nmcli con up enp0s8
```

>Interface WAN  

### B. DNS

```bash
[root@node1 network-scripts]$ vi ifcfg-enp0s3
```

>Ajout du DNS

```bash
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp                                    # la carte configurera automatiquement au boot de la machine une adresse ip fourni par le DHCP(par opposition à statique)
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s3                                       # nom de l'interface
UUID=20c3d48c-1ae0-46c4-b839-2278901a5fdd
DEVICE=enp0s3                                     # nom de l'interface
ONBOOT=yes                                        # la carte s'allumera automatiquement au boot de la machine
DNS=1.1.1.1                                       # DNS choisi
```

```bash
[root@node1 network-scripts]$ nmcli con reload enp0s3
[root@node1 network-scripts]$ nmcli con up enp0s3
```

```bash
[root@node1 ~]$ ping google.com
PING google.com (142.250.185.78) 56(84) bytes of data.
64 bytes from fra16s48-in-f14.1e100.net (142.250.185.78): icmp_seq=1 ttl=114 time=43.10 ms
64 bytes from fra16s48-in-f14.1e100.net (142.250.185.78): icmp_seq=2 ttl=114 time=29.8 ms
64 bytes from fra16s48-in-f14.1e100.net (142.250.185.78): icmp_seq=3 ttl=114 time=29.4 ms
64 bytes from fra16s48-in-f14.1e100.net (142.250.185.78): icmp_seq=4 ttl=114 time=29.9 ms
64 bytes from fra16s48-in-f14.1e100.net (142.250.185.78): icmp_seq=5 ttl=114 time=29.1 ms
64 bytes from fra16s48-in-f14.1e100.net (142.250.185.78): icmp_seq=6 ttl=114 time=29.8 ms
^C
--- google.com ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 9089ms
rtt min/avg/max/mdev = 29.122/32.002/43.977/5.363 ms
```

### C. Hostname

```bash
[root@node1 ~]$ sudo hostname node1.tp1.cesi
[root@node1 ~]$ echo 'node1.tp1.cesi' | sudo tee /etc/hostname
```

```bash
[root@node1 ~]$ hostname
node1.tp1.cesi
```


```bash
Rocky Linux 8.5 (Green Obsidian)
Kernel 4.18.0-348.e18.0.2.x86_64

Activate the web console with: systemctl enable --now cockpit.socket

Hint: Num Lock on

node1 login :
```

## 2. Utilisateurs

```bash
[root@node1 ~]$ visudo
```

> Affiche la ligne qui donne tous les droits au groupe `wheel`

```bash
[root@node1 ~]$ cat /etc/sudoers | grep wheel
## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL
```

[comment]: <> test

```bash
[root@node1 ~]$ usermod -aG wheel yoann
passwd yoann
Changing password for user yoann.
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
```

```bash
[root@node1 ~]$ groups yoann
yoann : yoann wheel
```

```bash
sudo -s
[sudo] password for yoann:
[root@node1 yoann ~]$
```

## 3. SSH

```bash
ssh yoann@10.1.1.20
yoann@10.2.1.20's password:

Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Dec 15 19:29:34 2021 from 10.1.1.1
```

## 4. SELinux

```bash

[yoann@node1 ~]$ sestatus

SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   enforcing
Mode from config file:          enforcing
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      33
```

```bash
[yoann@node1 ~]$ sudo vi /etc/selinux/config
[sudo] password for yoann:
```

```bash
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=permissive
# SELINUXTYPE= can take one of these three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted
```

```bash
[yoann@node1 ~]$ sestatus

SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   permissive
Mode from config file:          permissive
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      33
```